﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class OrderPresentationTests
    {
        BindingList<Goods> _productList;
        BindingList<string> _productClass;
        BindingList<Goods> _testList;
        OrderModel _orderModel;
        OrderPresentation _orderPresentation;

        /// Test Initialize
        [TestInitialize()]
        public void OrderPresentationTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            _productClass = menuModel.GetGoodsClass();
            _testList = new BindingList<Goods>(_productList.Where(product => product.GetClassName == "主機板").ToList());
            _orderModel = new OrderModel(_productList, _productClass);
            _orderPresentation = new OrderPresentation(_orderModel);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test SetPageString
        [TestMethod()]
        public void SetPageStringTest()
        {
            Assert.AreEqual(_orderModel.SetPageString(1, _testList), _orderPresentation.SetPageString(1, _testList));
            Assert.AreEqual(_orderModel.SetPageString(2, _testList), _orderPresentation.SetPageString(2, _testList));
            Assert.AreEqual(_orderModel.SetPageString(3, _testList), _orderPresentation.SetPageString(3, _testList));

        }

        /// Test IsNextButtonEnable
        [TestMethod()]
        public void IsNextButtonEnableTest()
        {
            Assert.AreEqual(_orderModel.CheckOnePagesOverSix(_testList, 1), _orderPresentation.IsNextButtonEnable(_testList, 1));
            Assert.AreEqual(_orderModel.CheckOnePagesOverSix(_testList, 2), _orderPresentation.IsNextButtonEnable(_testList, 2));
            Assert.AreEqual(_orderModel.CheckOnePagesOverSix(_testList, 3), _orderPresentation.IsNextButtonEnable(_testList, 3));
        }

        /// Test IsBacjButtonEnable
        [TestMethod()]
        public void IsBackButtonEnableTest()
        {
            Assert.AreEqual(_orderModel.IsBackButtonEnable(1), _orderPresentation.IsBackButtonEnable(1));
            Assert.AreEqual(_orderModel.IsBackButtonEnable(2), _orderPresentation.IsBackButtonEnable(2));
        }

        /// Test IsAddButtonEnable
        [TestMethod()]
        public void IsAddButtonEnableTest()
        {
            Assert.IsFalse(_orderPresentation.IsAddButtonEnable());
        }

        /// Test CountLocalIndexText
        [TestMethod()]
        public void CountLocalIndexTest()
        {
            Assert.AreEqual(_orderModel.CountLocalIndex(1, 6), _orderPresentation.CountLocalIndex(1, 6));
            Assert.AreEqual(_orderModel.CountLocalIndex(2, 1), _orderPresentation.CountLocalIndex(2, 1));
            Assert.AreEqual(_orderModel.CountLocalIndex(1, 5), _orderPresentation.CountLocalIndex(1, 5));
        }

        /// TestTotalPriceString
        [TestMethod()]
        public void TotalPriceStringTest()
        {
            Assert.AreEqual("總價 : $1,000", _orderPresentation.TotalPriceString(1000));
            Assert.AreEqual("總價 : $900", _orderPresentation.TotalPriceString(900));
            Assert.AreEqual("總價 : $1,234,567", _orderPresentation.TotalPriceString(1234567));
        }

        /// Test IsPaymentButtonEnable
        [TestMethod()]
        public void IsPaymentButtonEnableTest()
        {
            Assert.AreEqual(_orderModel.IsPaymentButtonEnable(0), _orderPresentation.IsPaymentButtonEnable(0));
            Assert.AreEqual(_orderModel.IsPaymentButtonEnable(-1), _orderPresentation.IsPaymentButtonEnable(-1));
            Assert.AreEqual(_orderModel.IsPaymentButtonEnable(100), _orderPresentation.IsPaymentButtonEnable(100));

        }

        /// Test ResetAllProductInCart
        [TestMethod()]
        public void ResetAllProductInCartTest()
        {
            _testList[0].GetInCartStatus = true;
            _orderPresentation.ResetAllProductInCart(_testList);
            foreach (Goods goods in _testList)
                Assert.IsFalse(goods.GetInCartStatus);
        }

        /// Test GetGoodsList
        [TestMethod()]
        public void GetGoodsListTest()
        {
            Assert.AreEqual(_productList, _orderPresentation.GetGoodsList());
        }

        /// Test GetGoodsClass
        [TestMethod()]
        public void GetGoodsClassTest()
        {
            Assert.AreEqual(_productClass, _orderPresentation.GetGoodsClass());
        }

        /// Test GetRowsTotalPrice
        [TestMethod()]
        public void GetRowsTotalPriceTest()
        {
            Assert.AreEqual("$1,000", _orderPresentation.GetRowsTotalPrice(20.ToString(), 50.ToString()));
        }

        /// Test SetAddButtonEnable
        [TestMethod()]
        public void SetAddButtonEnableTest()
        {
            Goods product = null;
            foreach (Goods goods in _productList)
                if (goods.GetName == "微星 PRISTIGE X570 CREATION 主機板")
                    product = goods;
            product.GetInCartStatus = true;
            Assert.IsTrue(product.GetInCartStatus);
            _orderPresentation.SetAddButtonEnable("微星 PRISTIGE X570 CREATION 主機板");
            Assert.IsFalse(product.GetInCartStatus);
        }

        /// Test IsAutoGenerateColumns
        [TestMethod()]
        public void IsAutoGenerateColumnsTest()
        {
            Assert.IsFalse(_orderPresentation.IsAutoGenerateColumns());
        }

        /// Test GetCartTotalPrice
        [TestMethod()]
        public void GetCartTotalPriceTest()
        {
            Assert.AreEqual(_orderModel.GetCartTotalPrice(_testList), _orderPresentation.GetCartTotalPrice(_testList));
        }

        /// Test IsChangeToPreviousPages
        [TestMethod()]
        public void IsChangeToPreviousPagesTest()
        {
            Assert.AreEqual(_orderModel.IsChangeToPreviousPages(2, _testList), _orderPresentation.IsChangeToPreviousPages(2, _testList));
        }
    }
}
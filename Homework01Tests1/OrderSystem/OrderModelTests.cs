﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class OrderModelTests
    {
        BindingList<Goods> _productList;
        BindingList<string> _productClass;
        BindingList<Goods> _testList;
        OrderModel _orderModel;

        /// Test Initialize
        [TestInitialize()]
        public void OrderModelTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            _productClass = menuModel.GetGoodsClass();
            _testList = new BindingList<Goods>(_productList.Where(product => product.GetClassName == "主機板").ToList());
            _orderModel = new OrderModel(_productList, _productClass);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test CheckOnePagesOverSix
        [TestMethod()]
        public void CheckOnePagesOverSixTest()
        {
            BindingList<Goods> _testBindingList = new BindingList<Goods>(_productList.Where(product => product.GetClassName == "主機板").ToList());
            int pagesTest1 = 1;
            Assert.IsTrue(_orderModel.CheckOnePagesOverSix(_testBindingList, pagesTest1));
            int pagesTest2 = 2;
            Assert.IsFalse(_orderModel.CheckOnePagesOverSix(_testBindingList, pagesTest2));
        }

        /// Test IsBackButtonEnable
        [TestMethod()]
        public void IsBackButtonEnableTest()
        {
            Assert.IsTrue(_orderModel.IsBackButtonEnable(2));
            Assert.IsFalse(_orderModel.IsBackButtonEnable(1));
        }

        /// Test SetPageString
        [TestMethod()]
        public void SetPageStringTest()
        {
            BindingList<Goods> _testBindingList = new BindingList<Goods>(_productList.Where(product => product.GetClassName == "主機板").ToList());
            Assert.AreEqual("Page : 1/2", _orderModel.SetPageString(1, _testBindingList));
            Assert.AreEqual("Page : 2/2", _orderModel.SetPageString(2, _testBindingList));
            Assert.AreEqual("Page : 0/0", _orderModel.SetPageString(3, _testBindingList));

        }

        /// Test IsPaymantButtonEnable
        [TestMethod()]
        public void IsPaymentButtonEnableTest()
        {
            Assert.IsTrue(_orderModel.IsPaymentButtonEnable(10));
            Assert.IsFalse(_orderModel.IsPaymentButtonEnable(0));
        }

        /// Test CountLocalIndex
        [TestMethod()]
        public void CountLocalIndexTest()
        {
            Assert.AreEqual(6, _orderModel.CountLocalIndex(1, 6));
            Assert.AreEqual(7, _orderModel.CountLocalIndex(2, 1));
            Assert.ThrowsException<ArgumentException>(()=>_orderModel.CountLocalIndex(0, 1));
        }

        /// Test GetGoodList
        [TestMethod()]
        public void GetGoodsListTest()
        {
            Assert.AreEqual(_productList, _orderModel.GetGoodsList());
        }

        /// Test GetGoodsClass
        [TestMethod()]
        public void GetGoodsClassTest()
        {
            Assert.AreEqual(_productClass, _orderModel.GetGoodsClass());
        }

        /// Test GetRowTotalPrice
        [TestMethod()]
        public void GetRowsTotalPriceTest()
        {
            Assert.AreEqual(1000, _orderModel.GetRowsTotalPrice(20.ToString(), 50.ToString()));
        }

        /// Test SetAddButtonEnable
        [TestMethod()]
        public void SetAddButtonEnableTest()
        {
            Goods product = null;
            foreach (Goods goods in _productList)
                if (goods.GetName == "微星 PRISTIGE X570 CREATION 主機板")
                    product = goods;
            product.GetInCartStatus = true;
            Assert.IsTrue(product.GetInCartStatus);
            _orderModel.SetAddButtonEnable("微星 PRISTIGE X570 CREATION 主機板");
            Assert.IsFalse(product.GetInCartStatus);
        }

        /// Test GetCartTotalPrice
        [TestMethod()]
        public void GetCartTotalPriceTest()
        {
            Assert.AreEqual(33530, _orderModel.GetCartTotalPrice(_testList));
        }

        /// Test IsChangeToPreviousPages
        [TestMethod()]
        public void IsChangeToPreviousPagesTest()
        {
            Assert.AreEqual(2, _orderModel.IsChangeToPreviousPages(2, _testList));
            _testList.RemoveAt(0);
            Assert.AreEqual(1, _orderModel.IsChangeToPreviousPages(2, _testList));
        }
    }
}
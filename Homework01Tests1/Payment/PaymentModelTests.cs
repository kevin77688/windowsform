﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework01.Tests
{
    [TestClass()]
    public class PaymentModelTests
    {
        PaymentModel _paymentModel;
        /// Test Initialize
        [TestInitialize()]
        public void PaymentModelTest()
        {
            _paymentModel = new PaymentModel();
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test RemoveLastCharacter
        [TestMethod()]
        public void RemoveLastCharacterTest()
        {
            Assert.AreEqual("tes", _paymentModel.RemoveLastCharacter("test"));
            Assert.AreEqual("test", _paymentModel.RemoveLastCharacter("testt"));

        }
    }
}
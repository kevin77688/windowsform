﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Homework01.Tests
{
    [TestClass()]
    public class PaymentPresentationTests
    {
        PaymentModel _paymentModel;
        PaymentPresentation _paymentPresentation;
        /// Test Initialize
        [TestInitialize()]
        public void PaymentPresentationTest()
        {
            _paymentModel = new PaymentModel();
            _paymentPresentation = new PaymentPresentation(_paymentModel);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test IsCheckOutEnable
        [TestMethod()]
        public void IsCheckOutEnableTest()
        {
            Assert.IsFalse(_paymentPresentation.IsCheckOutEnable());
        }

        /// Test IsInputNumberTest
        [TestMethod()]
        public void IsInputNumberTest()
        {
            Assert.IsFalse(_paymentPresentation.IsInputNumber('0'));
            Assert.IsFalse(_paymentPresentation.IsInputNumber('5'));
            Assert.IsTrue(_paymentPresentation.IsInputNumber('a'));
        }

        /// Test IsENglishOrChinese
        [TestMethod()]
        public void IsEnglishOrChineseTest()
        {
            Assert.IsTrue(_paymentPresentation.IsEnglishOrChinese((char)8));
            Assert.IsTrue(_paymentPresentation.IsEnglishOrChinese('a'));
            Assert.IsTrue(_paymentPresentation.IsEnglishOrChinese('Z'));
            Assert.IsTrue(_paymentPresentation.IsEnglishOrChinese('測'));
            Assert.IsFalse(_paymentPresentation.IsEnglishOrChinese(';'));
        }

        /// Test IsTextBoxLengthOverflow
        [TestMethod()]
        public void IsTextBoxLengthOverflowTest()
        {
            Assert.IsTrue(_paymentPresentation.IsTextBoxLengthOverflow(5, 4));
            Assert.IsFalse(_paymentPresentation.IsTextBoxLengthOverflow(4, 4));
            Assert.IsFalse(_paymentPresentation.IsTextBoxLengthOverflow(3, 4));
        }

        /// Test CheckRemoveListCharacter
        [TestMethod()]
        public void CheckRemoveLastCharacterTest()
        {
            Assert.AreEqual("test", _paymentPresentation.CheckRemoveLastCharacter(4, 4, "test"));
            Assert.AreEqual("test", _paymentPresentation.CheckRemoveLastCharacter(5, 4, "testt"));
            Assert.AreEqual("correct", _paymentPresentation.CheckRemoveLastCharacter(7, 8, "correct"));
        }

        /// Test IsNextTextboxFocus
        [TestMethod()]
        public void IsNextTextBoxFocusTest()
        {
            Assert.IsTrue(_paymentPresentation.IsNextTextBoxFocus(true, 4));
            Assert.IsFalse(_paymentPresentation.IsNextTextBoxFocus(true, 3));
            Assert.IsFalse(_paymentPresentation.IsNextTextBoxFocus(false, 4));
            Assert.IsFalse(_paymentPresentation.IsNextTextBoxFocus(false, 3));
        }

        /// Test IsNameTextBoxNull
        [TestMethod()]
        public void IsNameTextBoxNullTest()
        {
            Assert.IsTrue(_paymentPresentation.IsNameTextBoxNull(0));
            Assert.IsFalse(_paymentPresentation.IsNameTextBoxNull(1));
        }

        /// Test IsNumberTextBoxCorrect
        [TestMethod()]
        public void IsNumberTextBoxCorrectTest()
        {
            Assert.IsTrue(_paymentPresentation.IsNumberTextBoxCorrect(5.ToString(), 4));
            Assert.IsFalse(_paymentPresentation.IsNumberTextBoxCorrect(5.ToString(), 5));
            Assert.IsFalse(_paymentPresentation.IsNumberTextBoxCorrect(4.ToString(), 4));
        }

        /// Test IsComboBoxSelectNull
        [TestMethod()]
        public void IsComboBoxSelectNullTest()
        {
            Assert.IsTrue(_paymentPresentation.IsComboBoxSelectNull(1));
            Assert.IsTrue(_paymentPresentation.IsComboBoxSelectNull(0));
            Assert.IsFalse(_paymentPresentation.IsComboBoxSelectNull(-1));
        }

        /// Test IsMailValid
        [TestMethod()]
        public void IsMailValidTest()
        {
            Assert.IsTrue(_paymentPresentation.IsMailValid("a@g.c"));
            Assert.IsFalse(_paymentPresentation.IsMailValid("a@abc"));
            Assert.IsFalse(_paymentPresentation.IsMailValid("a!abc"));
            Assert.IsFalse(_paymentPresentation.IsMailValid("a@!abc"));
        }

        /// Test IsWordCorrect
        [TestMethod()]
        public void IsWordCorrectTest()
        {
            Assert.IsFalse(_paymentPresentation.IsWordCorrect(2));
            Assert.IsFalse(_paymentPresentation.IsWordCorrect(5));
            Assert.IsTrue(_paymentPresentation.IsWordCorrect(0));
        }

        /// Test IsNumberCorrect
        [TestMethod()]
        public void IsNumberCorrectTest()
        {
            Assert.IsFalse(_paymentPresentation.IsNumberCorrect(5, 4.ToString()));
            Assert.IsFalse(_paymentPresentation.IsNumberCorrect(5, 5.ToString()));
            Assert.IsFalse(_paymentPresentation.IsNumberCorrect(4, 4.ToString()));
            Assert.IsTrue(_paymentPresentation.IsNumberCorrect(3, 4.ToString()));
        }

        /// Test SaveFIle
        [TestMethod()]
        public void SaveFileTest()
        {
            XmlDocument _lastData = new XmlDocument();
            _lastData.Load(@"../../CreditCard.xml");
            List<string> data = new List<string>
            {
                "firstName",
                "lastName",
                "1234",
                "1234",
                "1234",
                "1234",
                "1",
                "3",
                "a@g.cx",
                "address"
            };
            Assert.AreEqual(_paymentModel.SaveFile(data, _lastData), _paymentPresentation.SaveFile(data, _lastData));
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Homework01.Tests
{
    [TestClass()]
    public class GoodsTests
    {
        Goods _goods;
        PrivateObject _target;
        /// Test Initialize
        [TestInitialize()]
        public void GoodsTest()
        {
            _goods = new Goods("測試類別", "測試物品", "測試細節", 10, @"../../../Homework01/Assest/productPicture/computer1.jpg", 10);
            _target = new PrivateObject(_goods);
        }

        /// Test Cleanup
        [TestCleanup]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        /// Test GetProductPrice
        [TestMethod]
        public void GetProductPriceString()
        {
            string expect = "售價 : $10";
            Assert.AreEqual(expect, _target.GetFieldOrProperty("GetProductPriceString"));
        }

        /// Test GetInCartStatus
        [TestMethod]
        public void GetInCartStatusTest()
        {
            Assert.IsFalse((bool)_target.GetFieldOrProperty("GetInCartStatus"));
            _goods.GetInCartStatus = true;
            Assert.IsTrue((bool)_target.GetFieldOrProperty("GetInCartStatus"));
        }

        /// Test GetTotalRowPrice
        [TestMethod]
        public void GetTotalRowPriceTest()
        {
            Assert.AreEqual("$10", _target.GetFieldOrProperty("GetTotalRowPrice"));
            _goods.GetLocalQuantity = 2;
            Assert.AreEqual("$20", _target.GetFieldOrProperty("GetTotalRowPrice"));
            _goods.GetPrice = 1000;
            Assert.AreEqual("$2,000", _target.GetFieldOrProperty("GetTotalRowPrice"));
        }

        /// Test IsButtonEnable
        [TestMethod]
        public void IsButtonEnableTest()
        {
            Assert.IsTrue((bool)_target.GetFieldOrProperty("IsButtonEnable"));
            _goods.GetQuantity = 0;
            Assert.IsFalse((bool)_target.GetFieldOrProperty("IsButtonEnable"));
            _goods.GetQuantity = 5;
            _goods.GetInCartStatus = true;
            Assert.IsFalse((bool)_target.GetFieldOrProperty("IsButtonEnable"));
        }

        /// Test GetLocalQuantity
        [TestMethod()]
        public void GetLocalQuantityTest()
        {
            Assert.AreEqual(1, _target.GetFieldOrProperty("GetLocalQuantity"));
            _goods.GetLocalQuantity = 2;
            Assert.AreEqual(2, _target.GetFieldOrProperty("GetLocalQuantity"));
            _goods.GetQuantity = 1;
            _goods.GetLocalQuantity = 3;
            Assert.AreEqual(1, _target.GetFieldOrProperty("GetLocalQuantity"));
        }

        /// Test GetTotalRowPriceNumber
        [TestMethod]
        public void GetTotalRowPriceNumberTest()
        {
            Assert.AreEqual(10, _target.GetFieldOrProperty("GetTotalRowPriceNumber"));
            _goods.GetLocalQuantity = 2;
            Assert.AreEqual(20, _target.GetFieldOrProperty("GetTotalRowPriceNumber"));
            _goods.GetPrice = 1000;
            Assert.AreEqual(2000, _target.GetFieldOrProperty("GetTotalRowPriceNumber"));
        }

        /// Test PurchaseProduct
        [TestMethod()]
        public void PurchaseProductTest()
        {
            Assert.AreEqual(10, _target.GetFieldOrProperty("GetQuantity"));
            Assert.AreEqual(1, _target.GetFieldOrProperty("GetLocalQuantity"));
            _goods.PurchaseProduct();
            Assert.AreEqual(9, _target.GetFieldOrProperty("GetQuantity"));
            Assert.AreEqual(1, _target.GetFieldOrProperty("GetLocalQuantity"));
        }

        /// Test SetProduct
        [TestMethod()]
        public void SetProductTest()
        {
            List<string> data = new List<string>();
            Bitmap originalPicture = _goods.GetPicture;
            data.Add("testName");
            data.Add("20");
            data.Add("testClass");
            data.Add(@"../../../Homework01/Assest/productPicture/computer2.jpg");
            data.Add("testDetail");
            Assert.AreEqual("測試物品", _target.GetFieldOrProperty("GetName"));
            Assert.AreEqual(10, _target.GetFieldOrProperty("GetPrice"));
            Assert.AreEqual("測試類別", _target.GetFieldOrProperty("GetClassName"));
            Assert.AreEqual(originalPicture, _target.GetFieldOrProperty("GetPicture"));
            Assert.AreEqual(@"../../../Homework01/Assest/productPicture/computer1.jpg", _target.GetFieldOrProperty("GetPicturePath"));
            Assert.AreEqual("測試細節", _target.GetFieldOrProperty("GetDetail"));
            _goods.SetProduct(data);
            Assert.AreEqual("testName", _target.GetFieldOrProperty("GetName"));
            Assert.AreEqual(20, _target.GetFieldOrProperty("GetPrice"));
            Assert.AreEqual("testClass", _target.GetFieldOrProperty("GetClassName"));
            Assert.AreNotEqual(originalPicture, _target.GetFieldOrProperty("GetPicture"));
            Assert.AreEqual(@"../../../Homework01/Assest/productPicture/computer2.jpg", _target.GetFieldOrProperty("GetPicturePath"));
            Assert.AreEqual("testDetail", _target.GetFieldOrProperty("GetDetail"));
        }

        /// Test IsModified
        [TestMethod()]
        public void IsModifiedTest()
        {
            List<string> data = new List<string>();
            Bitmap originalPicture = _goods.GetPicture;
            data.Add("測試物品");
            data.Add("10");
            data.Add("測試類別");
            data.Add(@"../../../Homework01/Assest/productPicture/computer1.jpg");
            data.Add("測試細節");
            Assert.IsTrue(_goods.IsModified(data));
            data[0] = "測試物";
            Assert.IsFalse(_goods.IsModified(data));
        }

        /// Test GetImage of buyImage and DeleteImage
        [TestMethod()]
        public void GetImageTest()
        {
            Assert.AreEqual(_goods.GetBuyImage.ToString(), _target.GetFieldOrProperty("GetBuyImage").ToString());
            Assert.AreEqual(_goods.GetDeleteImage.ToString(), _target.GetFieldOrProperty("GetDeleteImage").ToString());
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework01.Tests
{
    [TestClass()]
    public class MenuModelTests
    {
        MenuModel _menuModel;
        [TestInitialize()]
        public void MenuModelTest()
        {
            _menuModel = new MenuModel();
        }
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        [TestMethod()]
        public void GetGoodsListTest()
        {
            int count = 38;
            Assert.AreEqual(count, _menuModel.GetGoodsList().Count);
        }

        [TestMethod()]
        public void GetGoodsClassTest()
        {
            int count = 6;
            Assert.AreEqual(count, _menuModel.GetGoodsClass().Count);
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework01.Tests
{
    [TestClass()]
    public class MenuPresentationTests
    {
        MenuModel _menuModel;
        MenuPresentation _menuPresentation;

        /// Test Initialize
        [TestInitialize()]
        public void MenuPresentationTest()
        {
            _menuModel = new MenuModel();
            _menuPresentation = new MenuPresentation(_menuModel);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test IsOrderSystemEnable
        [TestMethod()]
        public void IsOrderSystemEnableTest()
        {
            Assert.IsFalse(_menuPresentation.IsOrderSystemEnable());
        }

        /// Test Is OrderSystemClosed
        [TestMethod()]
        public void IsOrderSystemClosedTest()
        {
            Assert.IsTrue(_menuPresentation.IsOrderSystemClosed());
        }

        /// Test IsInventorySystemEnable
        [TestMethod()]
        public void IsInventorySystemEnableTest()
        {
            Assert.IsFalse(_menuPresentation.IsInventorySystemEnable());
        }

        /// Test IsInventorySystemClosed
        [TestMethod()]
        public void IsInventorySystemClosedTest()
        {
            Assert.IsTrue(_menuPresentation.IsInventorySystemClosed());
        }

        /// Test IsProductManagementSystemEnable
        [TestMethod()]
        public void IsProductManagementSystemEnableTest()
        {
            Assert.IsFalse(_menuPresentation.IsProductManagementSystemEnable());
        }

        /// Test IsProductManagementSystemClosed
        [TestMethod()]
        public void IsProductManagementSystemClosedTest()
        {
            Assert.IsTrue(_menuPresentation.IsProductManagementSystemClosed());
        }

        /// Test GetGoodsList
        [TestMethod()]
        public void GetGoodsListTest()
        {
            Assert.AreEqual(_menuModel.GetGoodsList(), _menuPresentation.GetGoodsList());
        }

        /// Test GetGoodsClass
        [TestMethod()]
        public void GetGoodsClassTest()
        {
            Assert.AreEqual(_menuModel.GetGoodsClass(), _menuPresentation.GetGoodsClass());
        }
    }
}
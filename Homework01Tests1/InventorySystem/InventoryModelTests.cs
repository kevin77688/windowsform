﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class InventoryModelTests
    {
        BindingList<Goods> _productList;
        BindingList<string> _productClass;
        InventoryModel _inventoryModel;

        /// Test Initialize
        [TestInitialize()]
        public void InventoryModelTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            _productClass = menuModel.GetGoodsClass();
            _inventoryModel = new InventoryModel(_productList);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test GetAllProductList
        [TestMethod()]
        public void GetAllProductListTest()
        {
            Assert.AreEqual(_productList, _inventoryModel.GetAllProductList());
        }
    }
}
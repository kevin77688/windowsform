﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class InventoryPresentationTests
    {
        BindingList<Goods> _productList;
        BindingList<string> _productClass;
        InventoryModel _inventoryModel;
        InventoryPresentation _inventoryPresentation;

        /// Test Initialize
        [TestInitialize()]
        public void InventoryModelTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            _productClass = menuModel.GetGoodsClass();
            _inventoryModel = new InventoryModel(_productList);
            _inventoryPresentation = new InventoryPresentation(_inventoryModel);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// GetAllProductList
        [TestMethod()]
        public void GetAllProductListTest()
        {
            Assert.AreEqual(_productList, _inventoryPresentation.GetAllProductList());
        }

        /// Test IsAutoGenerateColumns
        [TestMethod()]
        public void IsAutoGenerateColumnsTest()
        {
            Assert.IsFalse(_inventoryPresentation.IsAutoGenerateColumns());
        }
        
        /// Test IsSelectButtonValid
        [TestMethod()]
        public void IsSelectButtonValidTest()
        {
            Assert.IsTrue(_inventoryPresentation.IsSelectButtonValid(2, 4));
            Assert.IsTrue(_inventoryPresentation.IsSelectButtonValid(1, 4));
            Assert.IsFalse(_inventoryPresentation.IsSelectButtonValid(-1, 4));
            Assert.IsFalse(_inventoryPresentation.IsSelectButtonValid(1, 3));

        }
    }
}
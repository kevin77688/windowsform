﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class ProductManagementModelTests
    {
        private BindingList<Goods> _productList;
        private BindingList<string> _productClass;
        private ProductManagementModel _productManagementModel;
        /// Test Initialize
        [TestInitialize()]
        public void ProductManagementModelTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            _productClass = menuModel.GetGoodsClass();
            _productManagementModel = new ProductManagementModel(_productList, _productClass);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test GetAllProduct
        [TestMethod()]
        public void GetAllProductTest()
        {
            Assert.AreEqual(_productList, _productManagementModel.GetAllProduct);
            _productList[0].GetName = "測試名稱";
            _productManagementModel.GetAllProduct = _productList;
            Assert.AreEqual(_productList, _productManagementModel.GetAllProduct);
        }

        /// Test GetAllClassList
        [TestMethod()]
        public void GetAllClassListTest()
        {
            Assert.AreEqual(_productClass, _productManagementModel.GetProductClassList);
            _productManagementModel.GetProductClassList = _productClass;
            Assert.AreEqual(_productClass, _productManagementModel.GetProductClassList);
        }

        /// Test GetCurrentProduct
        [TestMethod()]
        public void GetCUrrentProductTest()
        {
            Assert.AreEqual(_productList[0], _productManagementModel.GetCurrentProduct("微星 PRISTIGE X570 CREATION 主機板"));
            Assert.AreEqual(null, _productManagementModel.GetCurrentProduct("空商品"));
        }

    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class ProductManagementPresentationTests
    {
        BindingList<Goods> _productList;
        BindingList<string> _productClass;
        ProductManagementModel _productManagementModel;
        ProductManagementPresentation _productManagementPresentation;
        /// Test Initialize
        [TestInitialize()]
        public void ProductManagementPresentationTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            _productClass = menuModel.GetGoodsClass();
            _productManagementModel = new ProductManagementModel(_productList, _productClass);
            _productManagementPresentation = new ProductManagementPresentation(_productManagementModel);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test GetAllProduct
        [TestMethod()]
        public void GetAllProductTest()
        {
            Assert.AreEqual(_productList, _productManagementPresentation.GetAllProduct);
            _productManagementPresentation.GetAllProduct = _productList;
            Assert.AreEqual(_productList, _productManagementPresentation.GetAllProduct);
        }

        /// Test GetProductClassList
        [TestMethod()]
        public void GetProductClassListTest()
        {
            Assert.AreEqual(_productClass, _productManagementPresentation.GetProductClassList);
            _productManagementPresentation.GetProductClassList = _productClass;
            Assert.AreEqual(_productClass, _productManagementPresentation.GetProductClassList);
        }

        /// Test GetCurrentProduct
        [TestMethod()]
        public void GetCurrentProductTest()
        {
            Assert.AreEqual(_productList[0], _productManagementPresentation.GetCurrentProduct("微星 PRISTIGE X570 CREATION 主機板"));
        }

        /// Test IsNameExist
        [TestMethod()]
        public void IsNameExistTest()
        {
            Assert.IsFalse(_productManagementPresentation.IsNameExist(_productList, "測試名稱", "測試名稱"));
            Assert.IsTrue(_productManagementPresentation.IsNameExist(_productList, "微星 PRISTIGE X570 CREATION 主機板"));
            Assert.IsFalse(_productManagementPresentation.IsNameExist(_productList, "測試名稱"));

        }

        /// Test IsOnlyInsertNumber
        [TestMethod()]
        public void IsOnlyInsertNumberTest()
        {
            Assert.IsTrue(_productManagementPresentation.IsOnlyInsertNumber('a'));
            Assert.IsTrue(_productManagementPresentation.IsOnlyInsertNumber('Z'));
            Assert.IsFalse(_productManagementPresentation.IsOnlyInsertNumber('0'));
            Assert.IsFalse(_productManagementPresentation.IsOnlyInsertNumber('1'));
            Assert.IsFalse(_productManagementPresentation.IsOnlyInsertNumber((char)8));
        }

        /// Test IsStringEmpty
        [TestMethod()]
        public void IsStringEmptyTest()
        {
            Assert.IsTrue(_productManagementPresentation.IsStringEmpty(String.Empty));
            Assert.IsFalse(_productManagementPresentation.IsStringEmpty("測試"));
        }

        /// Test IsButtonEnable
        [TestMethod()]
        public void IsButtonEnableTest()
        {
            Goods goods = _productManagementPresentation.GetCurrentProduct("微星 PRISTIGE X570 CREATION 主機板");
            List<string> data = new List<string>();
            data.Add(goods.GetName);
            data.Add(goods.GetPrice.ToString());
            data.Add(goods.GetClassName);
            data.Add(String.Empty);
            data.Add(goods.GetDetail);
            Assert.IsFalse(_productManagementPresentation.IsButtonEnable(goods, data));
            data[0] = "測試名稱";
            Assert.IsTrue(_productManagementPresentation.IsButtonEnable(goods, data));
            data[0] = String.Empty;
            Assert.IsFalse(_productManagementPresentation.IsButtonEnable(goods, data));
        }

        /// Test IsSaveClassButton
        [TestMethod()]
        public void IsSaveClassButtonTest()
        {
            string text = "測試類別";
            Assert.IsTrue(_productManagementPresentation.IsSaveClassButtonEnable(true, text));
            Assert.IsFalse(_productManagementPresentation.IsSaveClassButtonEnable(true, ""));
            Assert.IsFalse(_productManagementPresentation.IsSaveClassButtonEnable(true, "主機板"));
            Assert.IsFalse(_productManagementPresentation.IsSaveClassButtonEnable(false, text));
        }
    }
}
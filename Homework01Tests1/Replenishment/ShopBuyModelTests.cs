﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class ShopBuyModelTests
    {
        BindingList<Goods> _productList;
        ShopBuyModel _shopBuyModel;
        private readonly string NAME = "微星 PRISTIGE X570 CREATION 主機板";
        private readonly string CLASS = "主機板";
        private readonly string PRICE = "16690";
        private readonly string QUANTITY = "1";

        /// Test Initialize
        [TestInitialize()]
        public void ShopBuyModelTest()
        {
            MenuModel menuModel = new MenuModel();
            _productList = menuModel.GetGoodsList();
            List<string> data = new List<string>();
            data.Add(NAME);
            data.Add(CLASS);
            data.Add(PRICE);
            data.Add(QUANTITY);
            _shopBuyModel = new ShopBuyModel(data, _productList);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test GetName
        [TestMethod]
        public void GetNameTest()
        {
            Assert.AreEqual(NAME, _shopBuyModel.GetName());
        }

        /// Test GetClass
        [TestMethod]
        public void GetClassTest()
        {
            Assert.AreEqual(CLASS, _shopBuyModel.GetClass());
        }

        /// Test GetPrice
        [TestMethod]
        public void GetPriceTest()
        {
            Assert.AreEqual(int.Parse(PRICE), _shopBuyModel.GetPrice());
        }

        /// Test GetQuantity
        [TestMethod]
        public void GetQuantityTest()
        {
            Assert.AreEqual(int.Parse(QUANTITY), _shopBuyModel.GetQuantity());
        }

        /// Test SetProduct
        [TestMethod]
        public void SetQuantityTest()
        {
            Assert.AreEqual(1, _shopBuyModel.GetQuantity());
            _shopBuyModel.SetQuantity(2);
            Goods product = null;
            foreach (Goods goods in _productList)
                if (goods.GetName == NAME)
                    product = goods;
            Assert.AreEqual(3, product.GetQuantity);
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01.Tests
{
    [TestClass()]
    public class ShopBuyPresentationTests
    {
        BindingList<Goods> _productList;
        ShopBuyModel _shopBuyModel;
        ShopBuyPresentation _shopBuyPresentation;
        private readonly string NAME = "微星 PRISTIGE X570 CREATION 主機板";
        private readonly string CLASS = "主機板";
        private readonly string PRICE = "16690";
        private readonly string QUANTITY = "1";
        /// Test Initialize
        [TestInitialize()]
        public void ShopBuyPresentationTest()
        {
            MenuModel menuModel = new MenuModel();
            List<string> data = new List<string>();
            data.Add(NAME);
            data.Add(CLASS);
            data.Add(PRICE);
            data.Add(QUANTITY);
            _productList = menuModel.GetGoodsList();
            _shopBuyModel = new ShopBuyModel(data, _productList);
            _shopBuyPresentation = new ShopBuyPresentation(_shopBuyModel);
        }

        /// Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// Test GetName
        [TestMethod()]
        public void GetNameTest()
        {
            Assert.AreEqual(_shopBuyModel.GetName(), _shopBuyPresentation.GetName());
        }

        /// Test GetClass
        [TestMethod()]
        public void GetClassTest()
        {
            Assert.AreEqual(_shopBuyModel.GetClass(), _shopBuyPresentation.GetClass());
        }

        /// Test GetPrice
        [TestMethod()]
        public void GetPriceTest()
        {
            Assert.AreEqual(_shopBuyModel.GetPrice(), _shopBuyPresentation.GetPrice());
        }

        /// Test GetQuantity
        [TestMethod()]
        public void GetQuantityTest()
        {
            Assert.AreEqual(_shopBuyModel.GetQuantity(), _shopBuyPresentation.GetQuantity());
        }

        /// Test SetQuantity
        [TestMethod()]
        public void SetQuantityTest()
        {
            Assert.AreEqual(1, _shopBuyModel.GetQuantity());
            _shopBuyPresentation.SetQuantity(2);
            Goods product = null;
            foreach (Goods goods in _productList)
                if (goods.GetName == NAME)
                    product = goods;
            Assert.AreEqual(3, product.GetQuantity);
        }
    }
}
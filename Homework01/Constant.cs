﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework01
{
    public class Constant
    {
        // Number
        public const int NUMBER_ZERO = 0;
        public const int NUMBER_ONE = 1;
        public const int NUMBER_TWO = 2;
        public const int NUMBER_THREE = 3;
        public const int NUMBER_FOUR = 4;
        public const int NUMBER_FIVE = 5;
        public const int NUMBER_SIX = 6;
        public const int NUMBER_SEVEN = 7;
        public const int NUMBER_EIGHT = 8;
        public const int NUMBER_NINE = 9;
        public const int CHARACTER_LOWER_A_CODE = 97;
        public const int CHARACTER_LAST_LETTER_LOWER_CODE = 122;
        public const int CHARACTER_UPPER_A_CODE = 65;
        public const int CHARACTER_LAST_LETTER_UPPER_CODE = 90;
        public const int NUMBER_ONE_CODE = 49;
        public const int NUMBER_ZERO_CODE = 57;
        public const int CHARACTER_DASH_CODE = 45;
        // Number string
        public const string STRING_ONE = "1";
        public const string STRING_TWO = "2";
        public const string STRING_THREE = "3";
        public const string STRING_FOUR = "4";
        public const string STRING_FIVE = "5";
        public const string STRING_SIX = "6";
        public const string STRING_SEVEN = "7";
        // Sign
        public const string SLASH = "/";
        public const string NONE = "";
        // Default path string
        public const string DEFAULT_DATA_PATH = "\\Shopping.xml";
        public const string DEFAULT_CREDIT_CARD_DATA = "\\CreditCard.xml";
        public const string DEFAULT_PRODUCT_PATH = "//goods/product";
        public const string DEFAULT_TRASH_ICON_PATH = @"../../Assest/Icon/trashcan.bmp";
        public const string DEFAULT_BUY_IMAGE = @"../../Assest/Icon/replenishmentIcon.png";
        public const string DEFAULT_NO_IMAGE = @"../../Assest/Icon/noImage.png";
        public const string DEFAULT_TARGET_PATH = "\\Temp\\CreditCard.xml";
        // Path
        public const string FIRST_NAME_PATH = "//creditCard/firstName";
        public const string LAST_NAME_PATH = "//creditCard/lastName";
        public const string CREDIT_CARD_FIRST_NUMBER_PATH = "//creditCard/Number1";
        public const string CREDIT_CARD_SECOND_NUMBER_PATH = "//creditCard/Number2";
        public const string CREDIT_CARD_THIRD_NUMBER_PATH = "//creditCard/Number3";
        public const string CREDIT_CARD_FOURTH_NUMBER_PATH = "//creditCard/Number4";
        public const string CREDIT_CARD_MONTH = "//creditCard/Month";
        public const string CREDIT_CARD_YEAR = "//creditCard/Year";
        public const string PAYMENT_MAIL_PATH = "//creditCard/Email";
        public const string PAYMENT_ADDRESS_PATH = "//creditCard/Address";
        // Interface string
        public const string PRICE_NAME = "售價 : {0:C0}";
        public const string TOTAL_PRICE = "總價 : {0:C0}";
        public const string CHANGE_TO_CURRENCY = "{0:C0}";
        public const string QUANTITY_NAME = "庫存數量 : ";
        public const string QUANTITY_NOT_ENOUGH = "庫存不足 !";
        public const string ERROR_FIND_PRODUCT = "錯誤 : 找不到商品";
        public const string ERROR_NAME_EXIST = "錯誤 : 產品名稱不可以重複";
        public const string ERROR_NAME_EMPTY = "錯誤 : 商品名稱不可為空白";
        public const string PAGES = "Page : ";
        public const string MOTHER_BOARD = "主機板";
        public const string CENTRAL_PROCESS_UNIT = "CPU";
        public const string MEMORY = "記憶體";
        public const string DISK = "硬碟";
        public const string GRAPHIC = "顯示卡";
        public const string COMPUTER = "套裝電腦";
        public const string NEW_PRODUCT = "新增";
        public const string ADD_NEW_PRODUCT = "新增商品";
        public const string ADD_NEW_CLASS = "新增類別";
        public const string MODIFY_CLASS = "類別";
        public const string MODIFY_PRODUCT = "儲存";
        public const string MODIFY_ORIGINAL_PRODUCT = "編輯商品";
        // Control string
        public const string CATEGORY = "category";
        public const string DETAIL = "detail";
        public const string PRICE = "price";
        public const string TEXT = "Text";
        public const string BACKGROUND = "BackgroundImage";
        public const string NAME = "name";
        public const string QUANTITY = "quantity";
        public const string GET_DETAIL = "GetDetail";
        public const string GET_PICTURE = "GetPicture";
        public const string GET_PICTURE_PATH = "GetPicturePath";
        public const string GET_NAME = "GetName";
        public const string GET_CLASS_NAME = "GetClassName";
        public const string GET_PRICE = "GetPrice";
        public const string GET_QUANTITY = "GetQuantity";
        public const string GET_IN_CART_STATUS = "GetInCartStatus";
        public const string GET_LOCAL_QUANTITY = "GetLocalQuantity";
        public const string GET_TOTAL_ROW_PRICE = "GetTotalRowPrice";
        public const string GET_TOTAL_ROW_PRICE_NUMBER = "GetTotalRowPriceNumber";
        public const string GET_PRODUCT_PRICE_STRING = "GetProductPriceString";
        public const string IS_BUTTON_ENABLE = "IsButtonEnable";
        public const string ENABLED = "enabled";
        public const string GET_BUY_ICON = "GetBuyImage";
        public const string GET_DELETE_ICON = "GetDeleteImage";
        public const string OPEN_FILE_FILTER = "Images (*.BMP;*.JPG,*.GIF,*.PNG,*.TIFF)|*.BMP;*.JPG;*.GIF;*.PNG;*.TIFF|" + "All files (*.*)|*.*";
        public const string NEXT_LINE = "\n";
        public const string ADDRESS = "address";
        // Check input string
        public const string PAYMENT_VALID = "訂購完成";
        public const string PAYMENT_INVALID = "訂購失敗";
        public const string MAIL_INPUT_ERROR = "Email 輸入格式錯誤";
        public const string ADDRESS_INPUT_ERROR = "地址輸入不能為空";
        public const string NAME_INPUT_ERROR = "姓名欄不可為空";
        public const string CHECK_INPUT_ERROR = "檢查碼輸入格式錯誤";
        public const string CREDIT_CARD_INPUT_ERROR = "信用卡卡號輸入格式錯誤";
        public const string DATE_INPUT_ERROR = "請選擇日期";
        // Exception
        public const string INPUT_ERROR_OVERLOAD = "輸入錯誤，頁數不可以大於目前頁數";
        public const string PAGE_BELOW_ONE = "輸入錯誤，頁數不可以小於 1";
    }
}

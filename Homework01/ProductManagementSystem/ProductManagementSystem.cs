﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;

namespace Homework01
{
    public partial class ProductManagementSystem : Form
    {
        private ProductManagementPresentation _productManagementPresentation;
        private BindingList<Goods> _allProduct;
        private BindingList<Goods> _currentSelectedClass;
        private OpenFileDialog _browsePicture;
        private Goods _currentProduct;
        private BindingList<string> _allClassName;
        private bool _isNewProduct = false;
        private bool _isNewClass = false;
        private string _originalName;
        public ProductManagementSystem(ProductManagementPresentation productManagementPresentation)
        {
            InitializeComponent();
            _productManagementPresentation = productManagementPresentation;
            _allProduct = _productManagementPresentation.GetAllProduct;
            _allClassName = _productManagementPresentation.GetProductClassList;
            _productListListBox.DataSource = _allProduct;
            _productListListBox.DisplayMember = Constant.GET_NAME;
            _productClassListBox.DataSource = _allClassName;
            _productListListBox.SelectedIndex = -1;
            _productClassListBox.SelectedIndex = -1;
            CleanForm();
            _productCategoryComboBox.DataSource = _allClassName;
            _productPicturePathTextBox.ReadOnly = true;
            _productCategoryComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _browsePicture = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = Constant.OPEN_FILE_FILTER,
                RestoreDirectory = true,
                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
        }

        /// Clean form while not selected
        private void CleanForm()
        {
            _productNameTextBox.Text = String.Empty;
            _productPriceTextBox.Text = String.Empty;
            _productCategoryComboBox.Text = string.Empty;
            _productPicturePathTextBox.Text = String.Empty;
            _editProductDetailTextBox.Text = String.Empty;
            _productSaveDataButton.Enabled = false;
            _saveClassButton.Enabled = false;
            _classNameTextBox.Text = string.Empty;
            _productClassListBox.SelectedIndex = -1;
        }

        /// Do action while change listbox selected
        private void CheckOriginNameAndSetDetail(object sender, EventArgs e)
        {
            _isNewProduct = false;
            _productCategoryComboBox.Enabled = true;
            _addNewProduct.Enabled = true;
            _productPicturePathTextBox.Text = String.Empty;
            _productSaveDataButton.Text = Constant.MODIFY_PRODUCT;
            _editProductGroupBox.Text = Constant.MODIFY_ORIGINAL_PRODUCT;
            if ((Goods)_productListListBox.SelectedItem != null)
            {
                _originalName = ((Goods)_productListListBox.SelectedItem).GetName;
                _currentProduct = (Goods)_productListListBox.SelectedItem;
                _productNameTextBox.Text = _currentProduct.GetName;
                _productPriceTextBox.Text = _currentProduct.GetPrice.ToString();
                _productPicturePathTextBox.Text = _currentProduct.GetPicturePath;
                _productCategoryComboBox.Text = _currentProduct.GetClassName;
                _editProductDetailTextBox.Text = _currentProduct.GetDetail;
            }
        }

        /// While click browse file button reaction
        private void BrowseButtonClick(object sender, EventArgs e)
        {
            if (_browsePicture.ShowDialog() == DialogResult.OK)
            {
                _productPicturePathTextBox.Text = _browsePicture.FileName;
                _productSaveDataButton.Enabled = true;
            }
        }

        /// Save product list while click Save button
        private void ClickSaveButton(object sender, EventArgs e)
        {
            if (_isNewProduct)
                _originalName = null;
            if (!_productManagementPresentation.IsNameExist(_allProduct, _productNameTextBox.Text, _originalName))
            {
                if (_productNameTextBox.Text != String.Empty && _productPriceTextBox.Text != String.Empty)
                {
                    if (!_isNewProduct)
                        SetProduct();
                    else
                        AddNewProduct();
                }
                else
                    MessageBox.Show(Constant.ERROR_NAME_EMPTY);
            }
            else
                MessageBox.Show(Constant.ERROR_NAME_EXIST);
        }

        /// Set product all detail
        private void SetProduct()
        {
            List<string> data = new List<string>();
            data.Add(_productNameTextBox.Text);
            data.Add(_productPriceTextBox.Text);
            data.Add(_productCategoryComboBox.Text);
            data.Add(_productPicturePathTextBox.Text);
            data.Add(_editProductDetailTextBox.Text);
            _currentProduct.SetProduct(data);
        }

        /// Add new product to list
        private void AddNewProduct()
        {
            string name = _productNameTextBox.Text;
            int price = int.Parse(_productPriceTextBox.Text);
            string className = _productCategoryComboBox.Text;
            string picture = Constant.DEFAULT_NO_IMAGE;
            if (!_productManagementPresentation.IsStringEmpty(_productPicturePathTextBox.Text))
                picture = _productPicturePathTextBox.Text;
            string detail = _editProductDetailTextBox.Text;
            int quantity = 0;
            _allProduct.Add(new Goods(className, name, detail, price, picture, quantity));
            CleanForm();
        }

        /// Check only input number
        private void CheckOnlyInsertNumber(object sender, KeyPressEventArgs e)
        {
            e.Handled = _productManagementPresentation.IsOnlyInsertNumber(e.KeyChar);
        }

        /// Add product to _allProduct while click button
        private void ClickAddProductButton(object sender, EventArgs e)
        {
            _isNewProduct = true;
            _addNewProduct.Enabled = false;
            _productCategoryComboBox.Enabled = true;
            _editProductGroupBox.Text = Constant.ADD_NEW_PRODUCT;
            _productSaveDataButton.Text = Constant.NEW_PRODUCT;
            _productNameTextBox.Text = String.Empty;
            _productPriceTextBox.Text = String.Empty;
            _productCategoryComboBox.SelectedIndex = 0;
            _productPicturePathTextBox.Text = String.Empty;
            _editProductDetailTextBox.Text = String.Empty;
        }

        /// Check name and price empty or not
        private void CheckTextBoxEmptyOrNot(object sender, EventArgs e)
        {
            List<string> data = new List<string>();
            data.Add(_productNameTextBox.Text);
            data.Add(_productPriceTextBox.Text);
            data.Add(_productCategoryComboBox.Text);
            data.Add(_productPicturePathTextBox.Text);
            data.Add(_editProductDetailTextBox.Text);
            _productSaveDataButton.Enabled = _productManagementPresentation.IsButtonEnable(_currentProduct, data);
        }

        /// Do action while change class index
        private void ChangeProductClassList(object sender, EventArgs e)
        {
            _classNameTextBox.Text = (string)_productClassListBox.SelectedItem;
            if (_currentSelectedClass != null)
                _currentSelectedClass.Clear();
            _categoryGroupBox.Text = Constant.MODIFY_CLASS;
            _saveClassButton.Enabled = false;
            _addProductClassButton.Enabled = true;
            _isNewClass = false;
            _currentSelectedClass = new BindingList<Goods>(_allProduct.Where(product => product.GetClassName == (string)_productClassListBox.SelectedItem).ToList());
            _classProductListBox.DataSource = _currentSelectedClass;
            _classProductListBox.DisplayMember = Constant.GET_NAME;
        }

        /// Clean form while change main tab
        private void ChangeMenuIndex(object sender, EventArgs e)
        {
            CleanForm();
            _addProductClassButton.Enabled = true;
            _categoryGroupBox.Text = Constant.MODIFY_CLASS;
        }

        /// Do action while click add new class name
        private void AddNewClass(object sender, EventArgs e)
        {
            CleanForm();
            _isNewClass = true;
            _addProductClassButton.Enabled = false;
            _categoryGroupBox.Text = Constant.ADD_NEW_CLASS;
        }

        /// Check class text is null or not
        private void CheckClassInsert(object sender, EventArgs e)
        {
            _saveClassButton.Enabled = _productManagementPresentation.IsSaveClassButtonEnable(_isNewClass, _classNameTextBox.Text);
        }

        /// Add product class while click button
        private void AddProductClass(object sender, EventArgs e)
        {
            _allClassName.Add(_classNameTextBox.Text);
        }
    }
}
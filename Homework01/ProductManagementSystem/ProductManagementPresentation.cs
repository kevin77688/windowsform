﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01
{
    public class ProductManagementPresentation
    {
        private ProductManagementModel _productManagementModel;
        public ProductManagementPresentation(ProductManagementModel productManagementModel)
        {
            _productManagementModel = productManagementModel;
        }

        /// return product list in one list
        public BindingList<Goods> GetAllProduct
        {
            get
            {
                return _productManagementModel.GetAllProduct;
            }
            set
            {
                _productManagementModel.GetAllProduct = value;
            }
        }

        /// return product class name in list
        public BindingList<string> GetProductClassList
        {
            get
            {
                return _productManagementModel.GetProductClassList;
            }
            set
            {
                _productManagementModel.GetProductClassList = value;
            }
        }

        /// Get Current selected item by name
        public Goods GetCurrentProduct(string name)
        {
            return _productManagementModel.GetCurrentProduct(name);
        }

        /// Check Name Exist in list
        public bool IsNameExist(BindingList<Goods> productList, string name, string currentName = null)
        {
            if (currentName == name)
                return false;
            foreach (Goods product in productList)
                if (name == product.GetName)
                    return true;
            return false;
        }

        /// Check Only insert number or delete
        public bool IsOnlyInsertNumber(char character)
        {
            if (!char.IsControl(character) && !char.IsDigit(character))
                return true;
            return false;
        }

        /// Check is empty or not
        public bool IsStringEmpty(string name)
        {
            return name == string.Empty;
        }

        /// Check name and price empty and return bool
        public bool IsButtonEnable(Goods product, List<string> data)
        {
            if (data[0] == String.Empty || data[1] == String.Empty || data[Constant.NUMBER_FOUR] == String.Empty)
                return false;
            return !product.IsModified(data);
        }

        /// Check save class enable or not
        public bool IsSaveClassButtonEnable(bool newClass, string text)
        {
            return (newClass && !IsStringEmpty(text) && !GetProductClassList.Contains(text));
        }
    }
}

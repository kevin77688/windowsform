﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Drawing;

namespace Homework01
{
    public class ProductManagementModel
    {
        private BindingList<Goods> _allProductInList;
        private BindingList<string> _allProductClass;
        public ProductManagementModel(BindingList<Goods> productList, BindingList<string> productClass)
        {
            _allProductInList = productList;
            _allProductClass = productClass;
        }

        /// return product list in one list
        public BindingList<Goods> GetAllProduct
        {
            get
            {
                return _allProductInList;
            }
            set
            {
                _allProductInList = value;
            }
        }

        /// return product class name in list
        public BindingList<string> GetProductClassList
        {
            get
            {
                return _allProductClass;
            }
            set
            {
                _allProductClass = value;
            }
        }

        /// Get Current selected item by name
        public Goods GetCurrentProduct(string name)
        {
            foreach (Goods product in _allProductInList)
                if (product.GetName == name)
                    return product;
            return null;
        }
    }
}

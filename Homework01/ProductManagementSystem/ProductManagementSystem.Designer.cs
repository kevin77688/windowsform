﻿namespace Homework01
{
    partial class ProductManagementSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._productManagementSystemTitle = new System.Windows.Forms.Label();
            this._mainTabControl = new System.Windows.Forms.TabControl();
            this._productTabPage = new System.Windows.Forms.TabPage();
            this._productManagementTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._addNewProduct = new System.Windows.Forms.Button();
            this._productListListBox = new System.Windows.Forms.ListBox();
            this._productSaveDataButton = new System.Windows.Forms.Button();
            this._editProductGroupBox = new System.Windows.Forms.GroupBox();
            this._editProductTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._editProductListTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._editProductPriceTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._productPriceLabel = new System.Windows.Forms.Label();
            this._priceCounterLabel = new System.Windows.Forms.Label();
            this._productPriceTextBox = new System.Windows.Forms.TextBox();
            this._editProductCategoryTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._productCategoryLabel = new System.Windows.Forms.Label();
            this._productCategoryComboBox = new System.Windows.Forms.ComboBox();
            this._editProductNameTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._productNameLabel = new System.Windows.Forms.Label();
            this._productNameTextBox = new System.Windows.Forms.TextBox();
            this._editProductPictureTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._productPicturePathLabel = new System.Windows.Forms.Label();
            this._browsePictureButton = new System.Windows.Forms.Button();
            this._productPicturePathTextBox = new System.Windows.Forms.TextBox();
            this._editProductDetailGroupBox = new System.Windows.Forms.GroupBox();
            this._editProductDetailTextBox = new System.Windows.Forms.TextBox();
            this._categoryTabPage = new System.Windows.Forms.TabPage();
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._categoryGroupBox = new System.Windows.Forms.GroupBox();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._classNameLabel = new System.Windows.Forms.Label();
            this._classNameTextBox = new System.Windows.Forms.TextBox();
            this._categoryProductGroupBox = new System.Windows.Forms.GroupBox();
            this._classProductListBox = new System.Windows.Forms.ListBox();
            this._productClassListBox = new System.Windows.Forms.ListBox();
            this._addProductClassButton = new System.Windows.Forms.Button();
            this._saveClassButton = new System.Windows.Forms.Button();
            this._mainTableLayoutPanel.SuspendLayout();
            this._mainTabControl.SuspendLayout();
            this._productTabPage.SuspendLayout();
            this._productManagementTableLayoutPanel.SuspendLayout();
            this._editProductGroupBox.SuspendLayout();
            this._editProductTableLayoutPanel.SuspendLayout();
            this._editProductListTableLayoutPanel.SuspendLayout();
            this._editProductPriceTableLayoutPanel.SuspendLayout();
            this._editProductCategoryTableLayoutPanel.SuspendLayout();
            this._editProductNameTableLayoutPanel.SuspendLayout();
            this._editProductPictureTableLayoutPanel.SuspendLayout();
            this._editProductDetailGroupBox.SuspendLayout();
            this._categoryTabPage.SuspendLayout();
            this._tableLayoutPanel1.SuspendLayout();
            this._categoryGroupBox.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this._tableLayoutPanel3.SuspendLayout();
            this._categoryProductGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _mainTableLayoutPanel
            // 
            this._mainTableLayoutPanel.ColumnCount = 1;
            this._mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTableLayoutPanel.Controls.Add(this._productManagementSystemTitle, 0, 0);
            this._mainTableLayoutPanel.Controls.Add(this._mainTabControl, 0, 1);
            this._mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._mainTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._mainTableLayoutPanel.Name = "_mainTableLayoutPanel";
            this._mainTableLayoutPanel.RowCount = 2;
            this._mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._mainTableLayoutPanel.Size = new System.Drawing.Size(734, 300);
            this._mainTableLayoutPanel.TabIndex = 0;
            // 
            // _productManagementSystemTitle
            // 
            this._productManagementSystemTitle.AutoSize = true;
            this._productManagementSystemTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productManagementSystemTitle.Font = new System.Drawing.Font("Microsoft YaHei", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._productManagementSystemTitle.Location = new System.Drawing.Point(2, 0);
            this._productManagementSystemTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productManagementSystemTitle.Name = "_productManagementSystemTitle";
            this._productManagementSystemTitle.Size = new System.Drawing.Size(730, 45);
            this._productManagementSystemTitle.TabIndex = 0;
            this._productManagementSystemTitle.Text = "商品管理系統";
            this._productManagementSystemTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _mainTabControl
            // 
            this._mainTabControl.Controls.Add(this._productTabPage);
            this._mainTabControl.Controls.Add(this._categoryTabPage);
            this._mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTabControl.Location = new System.Drawing.Point(2, 47);
            this._mainTabControl.Margin = new System.Windows.Forms.Padding(2);
            this._mainTabControl.Name = "_mainTabControl";
            this._mainTabControl.SelectedIndex = 0;
            this._mainTabControl.Size = new System.Drawing.Size(730, 251);
            this._mainTabControl.TabIndex = 1;
            this._mainTabControl.SelectedIndexChanged += new System.EventHandler(this.ChangeMenuIndex);
            // 
            // _productTabPage
            // 
            this._productTabPage.Controls.Add(this._productManagementTableLayoutPanel);
            this._productTabPage.Location = new System.Drawing.Point(4, 22);
            this._productTabPage.Margin = new System.Windows.Forms.Padding(2);
            this._productTabPage.Name = "_productTabPage";
            this._productTabPage.Padding = new System.Windows.Forms.Padding(2);
            this._productTabPage.Size = new System.Drawing.Size(722, 225);
            this._productTabPage.TabIndex = 0;
            this._productTabPage.Text = "商品管理";
            this._productTabPage.UseVisualStyleBackColor = true;
            // 
            // _productManagementTableLayoutPanel
            // 
            this._productManagementTableLayoutPanel.ColumnCount = 2;
            this._productManagementTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this._productManagementTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this._productManagementTableLayoutPanel.Controls.Add(this._addNewProduct, 0, 1);
            this._productManagementTableLayoutPanel.Controls.Add(this._productListListBox, 0, 0);
            this._productManagementTableLayoutPanel.Controls.Add(this._productSaveDataButton, 1, 1);
            this._productManagementTableLayoutPanel.Controls.Add(this._editProductGroupBox, 1, 0);
            this._productManagementTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productManagementTableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this._productManagementTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._productManagementTableLayoutPanel.Name = "_productManagementTableLayoutPanel";
            this._productManagementTableLayoutPanel.RowCount = 2;
            this._productManagementTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._productManagementTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._productManagementTableLayoutPanel.Size = new System.Drawing.Size(718, 221);
            this._productManagementTableLayoutPanel.TabIndex = 0;
            // 
            // _addNewProduct
            // 
            this._addNewProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addNewProduct.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._addNewProduct.Location = new System.Drawing.Point(2, 189);
            this._addNewProduct.Margin = new System.Windows.Forms.Padding(2);
            this._addNewProduct.Name = "_addNewProduct";
            this._addNewProduct.Size = new System.Drawing.Size(283, 30);
            this._addNewProduct.TabIndex = 0;
            this._addNewProduct.Text = "新增商品";
            this._addNewProduct.UseVisualStyleBackColor = true;
            this._addNewProduct.Click += new System.EventHandler(this.ClickAddProductButton);
            // 
            // _productListListBox
            // 
            this._productListListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productListListBox.FormattingEnabled = true;
            this._productListListBox.ItemHeight = 12;
            this._productListListBox.Location = new System.Drawing.Point(2, 2);
            this._productListListBox.Margin = new System.Windows.Forms.Padding(2);
            this._productListListBox.Name = "_productListListBox";
            this._productListListBox.Size = new System.Drawing.Size(283, 183);
            this._productListListBox.TabIndex = 1;
            this._productListListBox.SelectedIndexChanged += new System.EventHandler(this.CheckOriginNameAndSetDetail);
            // 
            // _productSaveDataButton
            // 
            this._productSaveDataButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._productSaveDataButton.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._productSaveDataButton.Location = new System.Drawing.Point(618, 189);
            this._productSaveDataButton.Margin = new System.Windows.Forms.Padding(2);
            this._productSaveDataButton.Name = "_productSaveDataButton";
            this._productSaveDataButton.Size = new System.Drawing.Size(98, 30);
            this._productSaveDataButton.TabIndex = 2;
            this._productSaveDataButton.Text = "儲存";
            this._productSaveDataButton.UseVisualStyleBackColor = true;
            this._productSaveDataButton.Click += new System.EventHandler(this.ClickSaveButton);
            // 
            // _editProductGroupBox
            // 
            this._editProductGroupBox.Controls.Add(this._editProductTableLayoutPanel);
            this._editProductGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductGroupBox.Location = new System.Drawing.Point(289, 2);
            this._editProductGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this._editProductGroupBox.Name = "_editProductGroupBox";
            this._editProductGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this._editProductGroupBox.Size = new System.Drawing.Size(427, 183);
            this._editProductGroupBox.TabIndex = 3;
            this._editProductGroupBox.TabStop = false;
            this._editProductGroupBox.Text = "編輯商品";
            // 
            // _editProductTableLayoutPanel
            // 
            this._editProductTableLayoutPanel.ColumnCount = 1;
            this._editProductTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductTableLayoutPanel.Controls.Add(this._editProductListTableLayoutPanel, 0, 1);
            this._editProductTableLayoutPanel.Controls.Add(this._editProductNameTableLayoutPanel, 0, 0);
            this._editProductTableLayoutPanel.Controls.Add(this._editProductPictureTableLayoutPanel, 0, 2);
            this._editProductTableLayoutPanel.Controls.Add(this._editProductDetailGroupBox, 0, 3);
            this._editProductTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductTableLayoutPanel.Location = new System.Drawing.Point(2, 17);
            this._editProductTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._editProductTableLayoutPanel.Name = "_editProductTableLayoutPanel";
            this._editProductTableLayoutPanel.RowCount = 4;
            this._editProductTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._editProductTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._editProductTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._editProductTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductTableLayoutPanel.Size = new System.Drawing.Size(423, 164);
            this._editProductTableLayoutPanel.TabIndex = 0;
            // 
            // _editProductListTableLayoutPanel
            // 
            this._editProductListTableLayoutPanel.ColumnCount = 2;
            this._editProductListTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._editProductListTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._editProductListTableLayoutPanel.Controls.Add(this._editProductPriceTableLayoutPanel, 0, 0);
            this._editProductListTableLayoutPanel.Controls.Add(this._editProductCategoryTableLayoutPanel, 1, 0);
            this._editProductListTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductListTableLayoutPanel.Location = new System.Drawing.Point(2, 32);
            this._editProductListTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._editProductListTableLayoutPanel.Name = "_editProductListTableLayoutPanel";
            this._editProductListTableLayoutPanel.RowCount = 1;
            this._editProductListTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductListTableLayoutPanel.Size = new System.Drawing.Size(419, 26);
            this._editProductListTableLayoutPanel.TabIndex = 0;
            // 
            // _editProductPriceTableLayoutPanel
            // 
            this._editProductPriceTableLayoutPanel.ColumnCount = 3;
            this._editProductPriceTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this._editProductPriceTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductPriceTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._editProductPriceTableLayoutPanel.Controls.Add(this._productPriceLabel, 0, 0);
            this._editProductPriceTableLayoutPanel.Controls.Add(this._priceCounterLabel, 2, 0);
            this._editProductPriceTableLayoutPanel.Controls.Add(this._productPriceTextBox, 1, 0);
            this._editProductPriceTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductPriceTableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this._editProductPriceTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._editProductPriceTableLayoutPanel.Name = "_editProductPriceTableLayoutPanel";
            this._editProductPriceTableLayoutPanel.RowCount = 1;
            this._editProductPriceTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductPriceTableLayoutPanel.Size = new System.Drawing.Size(205, 22);
            this._editProductPriceTableLayoutPanel.TabIndex = 0;
            // 
            // _productPriceLabel
            // 
            this._productPriceLabel.AutoSize = true;
            this._productPriceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productPriceLabel.Location = new System.Drawing.Point(2, 0);
            this._productPriceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productPriceLabel.Name = "_productPriceLabel";
            this._productPriceLabel.Size = new System.Drawing.Size(70, 22);
            this._productPriceLabel.TabIndex = 0;
            this._productPriceLabel.Text = "商品價格(*)";
            this._productPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _priceCounterLabel
            // 
            this._priceCounterLabel.AutoSize = true;
            this._priceCounterLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._priceCounterLabel.Location = new System.Drawing.Point(174, 0);
            this._priceCounterLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._priceCounterLabel.Name = "_priceCounterLabel";
            this._priceCounterLabel.Size = new System.Drawing.Size(29, 22);
            this._priceCounterLabel.TabIndex = 1;
            this._priceCounterLabel.Text = "NTD";
            this._priceCounterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _productPriceTextBox
            // 
            this._productPriceTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productPriceTextBox.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._productPriceTextBox.Location = new System.Drawing.Point(76, 2);
            this._productPriceTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._productPriceTextBox.Multiline = true;
            this._productPriceTextBox.Name = "_productPriceTextBox";
            this._productPriceTextBox.Size = new System.Drawing.Size(94, 18);
            this._productPriceTextBox.TabIndex = 2;
            this._productPriceTextBox.TextChanged += new System.EventHandler(this.CheckTextBoxEmptyOrNot);
            this._productPriceTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckOnlyInsertNumber);
            // 
            // _editProductCategoryTableLayoutPanel
            // 
            this._editProductCategoryTableLayoutPanel.ColumnCount = 2;
            this._editProductCategoryTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this._editProductCategoryTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductCategoryTableLayoutPanel.Controls.Add(this._productCategoryLabel, 0, 0);
            this._editProductCategoryTableLayoutPanel.Controls.Add(this._productCategoryComboBox, 1, 0);
            this._editProductCategoryTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductCategoryTableLayoutPanel.Location = new System.Drawing.Point(211, 2);
            this._editProductCategoryTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._editProductCategoryTableLayoutPanel.Name = "_editProductCategoryTableLayoutPanel";
            this._editProductCategoryTableLayoutPanel.RowCount = 1;
            this._editProductCategoryTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductCategoryTableLayoutPanel.Size = new System.Drawing.Size(206, 22);
            this._editProductCategoryTableLayoutPanel.TabIndex = 1;
            // 
            // _productCategoryLabel
            // 
            this._productCategoryLabel.AutoSize = true;
            this._productCategoryLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productCategoryLabel.Location = new System.Drawing.Point(2, 0);
            this._productCategoryLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productCategoryLabel.Name = "_productCategoryLabel";
            this._productCategoryLabel.Size = new System.Drawing.Size(70, 22);
            this._productCategoryLabel.TabIndex = 0;
            this._productCategoryLabel.Text = "商品類別(*)";
            this._productCategoryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _productCategoryComboBox
            // 
            this._productCategoryComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productCategoryComboBox.FormattingEnabled = true;
            this._productCategoryComboBox.Location = new System.Drawing.Point(76, 2);
            this._productCategoryComboBox.Margin = new System.Windows.Forms.Padding(2);
            this._productCategoryComboBox.Name = "_productCategoryComboBox";
            this._productCategoryComboBox.Size = new System.Drawing.Size(128, 20);
            this._productCategoryComboBox.TabIndex = 1;
            this._productCategoryComboBox.SelectedIndexChanged += new System.EventHandler(this.CheckTextBoxEmptyOrNot);
            // 
            // _editProductNameTableLayoutPanel
            // 
            this._editProductNameTableLayoutPanel.ColumnCount = 2;
            this._editProductNameTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this._editProductNameTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductNameTableLayoutPanel.Controls.Add(this._productNameLabel, 0, 0);
            this._editProductNameTableLayoutPanel.Controls.Add(this._productNameTextBox, 1, 0);
            this._editProductNameTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductNameTableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this._editProductNameTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._editProductNameTableLayoutPanel.Name = "_editProductNameTableLayoutPanel";
            this._editProductNameTableLayoutPanel.RowCount = 1;
            this._editProductNameTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductNameTableLayoutPanel.Size = new System.Drawing.Size(419, 26);
            this._editProductNameTableLayoutPanel.TabIndex = 1;
            // 
            // _productNameLabel
            // 
            this._productNameLabel.AutoSize = true;
            this._productNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productNameLabel.Location = new System.Drawing.Point(2, 0);
            this._productNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productNameLabel.Name = "_productNameLabel";
            this._productNameLabel.Size = new System.Drawing.Size(76, 26);
            this._productNameLabel.TabIndex = 0;
            this._productNameLabel.Text = "商品名稱(*)";
            this._productNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _productNameTextBox
            // 
            this._productNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productNameTextBox.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._productNameTextBox.Location = new System.Drawing.Point(82, 2);
            this._productNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._productNameTextBox.Multiline = true;
            this._productNameTextBox.Name = "_productNameTextBox";
            this._productNameTextBox.Size = new System.Drawing.Size(335, 22);
            this._productNameTextBox.TabIndex = 1;
            this._productNameTextBox.TextChanged += new System.EventHandler(this.CheckTextBoxEmptyOrNot);
            // 
            // _editProductPictureTableLayoutPanel
            // 
            this._editProductPictureTableLayoutPanel.ColumnCount = 3;
            this._editProductPictureTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this._editProductPictureTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductPictureTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this._editProductPictureTableLayoutPanel.Controls.Add(this._productPicturePathLabel, 0, 0);
            this._editProductPictureTableLayoutPanel.Controls.Add(this._browsePictureButton, 2, 0);
            this._editProductPictureTableLayoutPanel.Controls.Add(this._productPicturePathTextBox, 1, 0);
            this._editProductPictureTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductPictureTableLayoutPanel.Location = new System.Drawing.Point(2, 62);
            this._editProductPictureTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._editProductPictureTableLayoutPanel.Name = "_editProductPictureTableLayoutPanel";
            this._editProductPictureTableLayoutPanel.RowCount = 1;
            this._editProductPictureTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._editProductPictureTableLayoutPanel.Size = new System.Drawing.Size(419, 26);
            this._editProductPictureTableLayoutPanel.TabIndex = 2;
            // 
            // _productPicturePathLabel
            // 
            this._productPicturePathLabel.AutoSize = true;
            this._productPicturePathLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productPicturePathLabel.Location = new System.Drawing.Point(2, 0);
            this._productPicturePathLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productPicturePathLabel.Name = "_productPicturePathLabel";
            this._productPicturePathLabel.Size = new System.Drawing.Size(96, 26);
            this._productPicturePathLabel.TabIndex = 0;
            this._productPicturePathLabel.Text = "商品圖片路徑(*)";
            this._productPicturePathLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _browsePictureButton
            // 
            this._browsePictureButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._browsePictureButton.Location = new System.Drawing.Point(368, 2);
            this._browsePictureButton.Margin = new System.Windows.Forms.Padding(2);
            this._browsePictureButton.Name = "_browsePictureButton";
            this._browsePictureButton.Size = new System.Drawing.Size(49, 22);
            this._browsePictureButton.TabIndex = 2;
            this._browsePictureButton.Text = "瀏覽";
            this._browsePictureButton.UseVisualStyleBackColor = true;
            this._browsePictureButton.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // _productPicturePathTextBox
            // 
            this._productPicturePathTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productPicturePathTextBox.Location = new System.Drawing.Point(102, 2);
            this._productPicturePathTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._productPicturePathTextBox.Name = "_productPicturePathTextBox";
            this._productPicturePathTextBox.Size = new System.Drawing.Size(262, 22);
            this._productPicturePathTextBox.TabIndex = 3;
            this._productPicturePathTextBox.TextChanged += new System.EventHandler(this.CheckTextBoxEmptyOrNot);
            // 
            // _editProductDetailGroupBox
            // 
            this._editProductDetailGroupBox.Controls.Add(this._editProductDetailTextBox);
            this._editProductDetailGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductDetailGroupBox.Location = new System.Drawing.Point(2, 92);
            this._editProductDetailGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this._editProductDetailGroupBox.Name = "_editProductDetailGroupBox";
            this._editProductDetailGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this._editProductDetailGroupBox.Size = new System.Drawing.Size(419, 70);
            this._editProductDetailGroupBox.TabIndex = 3;
            this._editProductDetailGroupBox.TabStop = false;
            this._editProductDetailGroupBox.Text = "商品介紹";
            // 
            // _editProductDetailTextBox
            // 
            this._editProductDetailTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editProductDetailTextBox.Font = new System.Drawing.Font("PMingLiU", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._editProductDetailTextBox.Location = new System.Drawing.Point(2, 17);
            this._editProductDetailTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._editProductDetailTextBox.Multiline = true;
            this._editProductDetailTextBox.Name = "_editProductDetailTextBox";
            this._editProductDetailTextBox.Size = new System.Drawing.Size(415, 51);
            this._editProductDetailTextBox.TabIndex = 0;
            this._editProductDetailTextBox.TextChanged += new System.EventHandler(this.CheckTextBoxEmptyOrNot);
            // 
            // _categoryTabPage
            // 
            this._categoryTabPage.Controls.Add(this._tableLayoutPanel1);
            this._categoryTabPage.Location = new System.Drawing.Point(4, 22);
            this._categoryTabPage.Margin = new System.Windows.Forms.Padding(2);
            this._categoryTabPage.Name = "_categoryTabPage";
            this._categoryTabPage.Padding = new System.Windows.Forms.Padding(2);
            this._categoryTabPage.Size = new System.Drawing.Size(722, 225);
            this._categoryTabPage.TabIndex = 1;
            this._categoryTabPage.Text = "類別管理";
            this._categoryTabPage.UseVisualStyleBackColor = true;
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 2;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this._tableLayoutPanel1.Controls.Add(this._categoryGroupBox, 1, 0);
            this._tableLayoutPanel1.Controls.Add(this._productClassListBox, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._addProductClassButton, 0, 1);
            this._tableLayoutPanel1.Controls.Add(this._saveClassButton, 1, 1);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this._tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 2;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(718, 221);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _categoryGroupBox
            // 
            this._categoryGroupBox.Controls.Add(this._tableLayoutPanel2);
            this._categoryGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._categoryGroupBox.Location = new System.Drawing.Point(289, 2);
            this._categoryGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this._categoryGroupBox.Name = "_categoryGroupBox";
            this._categoryGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this._categoryGroupBox.Size = new System.Drawing.Size(427, 183);
            this._categoryGroupBox.TabIndex = 4;
            this._categoryGroupBox.TabStop = false;
            this._categoryGroupBox.Text = "類別";
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 1;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Controls.Add(this._tableLayoutPanel3, 0, 0);
            this._tableLayoutPanel2.Controls.Add(this._categoryProductGroupBox, 0, 1);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(2, 17);
            this._tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 2;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(423, 164);
            this._tableLayoutPanel2.TabIndex = 0;
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 2;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel3.Controls.Add(this._classNameLabel, 0, 0);
            this._tableLayoutPanel3.Controls.Add(this._classNameTextBox, 1, 0);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this._tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 1;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(419, 45);
            this._tableLayoutPanel3.TabIndex = 0;
            // 
            // _classNameLabel
            // 
            this._classNameLabel.AutoSize = true;
            this._classNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._classNameLabel.Font = new System.Drawing.Font("Microsoft YaHei", 11F);
            this._classNameLabel.Location = new System.Drawing.Point(2, 0);
            this._classNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._classNameLabel.Name = "_classNameLabel";
            this._classNameLabel.Size = new System.Drawing.Size(86, 45);
            this._classNameLabel.TabIndex = 0;
            this._classNameLabel.Text = "類別名稱(*)";
            this._classNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _classNameTextBox
            // 
            this._classNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._classNameTextBox.Location = new System.Drawing.Point(101, 12);
            this._classNameTextBox.Margin = new System.Windows.Forms.Padding(11, 12, 11, 12);
            this._classNameTextBox.Name = "_classNameTextBox";
            this._classNameTextBox.Size = new System.Drawing.Size(307, 22);
            this._classNameTextBox.TabIndex = 1;
            this._classNameTextBox.TextChanged += new System.EventHandler(this.CheckClassInsert);
            // 
            // _categoryProductGroupBox
            // 
            this._categoryProductGroupBox.Controls.Add(this._classProductListBox);
            this._categoryProductGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._categoryProductGroupBox.Location = new System.Drawing.Point(2, 51);
            this._categoryProductGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this._categoryProductGroupBox.Name = "_categoryProductGroupBox";
            this._categoryProductGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this._categoryProductGroupBox.Size = new System.Drawing.Size(419, 111);
            this._categoryProductGroupBox.TabIndex = 1;
            this._categoryProductGroupBox.TabStop = false;
            this._categoryProductGroupBox.Text = "類別內產品";
            // 
            // _classProductListBox
            // 
            this._classProductListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._classProductListBox.FormattingEnabled = true;
            this._classProductListBox.ItemHeight = 12;
            this._classProductListBox.Location = new System.Drawing.Point(2, 17);
            this._classProductListBox.Margin = new System.Windows.Forms.Padding(2);
            this._classProductListBox.Name = "_classProductListBox";
            this._classProductListBox.Size = new System.Drawing.Size(415, 92);
            this._classProductListBox.TabIndex = 0;
            // 
            // _productClassListBox
            // 
            this._productClassListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productClassListBox.FormattingEnabled = true;
            this._productClassListBox.ItemHeight = 12;
            this._productClassListBox.Location = new System.Drawing.Point(2, 2);
            this._productClassListBox.Margin = new System.Windows.Forms.Padding(2);
            this._productClassListBox.Name = "_productClassListBox";
            this._productClassListBox.Size = new System.Drawing.Size(283, 183);
            this._productClassListBox.TabIndex = 2;
            this._productClassListBox.SelectedIndexChanged += new System.EventHandler(this.ChangeProductClassList);
            // 
            // _addProductClassButton
            // 
            this._addProductClassButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addProductClassButton.Font = new System.Drawing.Font("Microsoft YaHei", 14F);
            this._addProductClassButton.Location = new System.Drawing.Point(2, 189);
            this._addProductClassButton.Margin = new System.Windows.Forms.Padding(2);
            this._addProductClassButton.Name = "_addProductClassButton";
            this._addProductClassButton.Size = new System.Drawing.Size(283, 30);
            this._addProductClassButton.TabIndex = 3;
            this._addProductClassButton.Text = "新增類別";
            this._addProductClassButton.UseVisualStyleBackColor = true;
            this._addProductClassButton.Click += new System.EventHandler(this.AddNewClass);
            // 
            // _saveClassButton
            // 
            this._saveClassButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveClassButton.Font = new System.Drawing.Font("Microsoft YaHei", 14F);
            this._saveClassButton.Location = new System.Drawing.Point(618, 189);
            this._saveClassButton.Margin = new System.Windows.Forms.Padding(2);
            this._saveClassButton.Name = "_saveClassButton";
            this._saveClassButton.Size = new System.Drawing.Size(98, 30);
            this._saveClassButton.TabIndex = 5;
            this._saveClassButton.Text = "新增";
            this._saveClassButton.UseVisualStyleBackColor = true;
            this._saveClassButton.Click += new System.EventHandler(this.AddProductClass);
            // 
            // ProductManagementSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 300);
            this.Controls.Add(this._mainTableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ProductManagementSystem";
            this.Text = "ProductManagementSystem";
            this._mainTableLayoutPanel.ResumeLayout(false);
            this._mainTableLayoutPanel.PerformLayout();
            this._mainTabControl.ResumeLayout(false);
            this._productTabPage.ResumeLayout(false);
            this._productManagementTableLayoutPanel.ResumeLayout(false);
            this._editProductGroupBox.ResumeLayout(false);
            this._editProductTableLayoutPanel.ResumeLayout(false);
            this._editProductListTableLayoutPanel.ResumeLayout(false);
            this._editProductPriceTableLayoutPanel.ResumeLayout(false);
            this._editProductPriceTableLayoutPanel.PerformLayout();
            this._editProductCategoryTableLayoutPanel.ResumeLayout(false);
            this._editProductCategoryTableLayoutPanel.PerformLayout();
            this._editProductNameTableLayoutPanel.ResumeLayout(false);
            this._editProductNameTableLayoutPanel.PerformLayout();
            this._editProductPictureTableLayoutPanel.ResumeLayout(false);
            this._editProductPictureTableLayoutPanel.PerformLayout();
            this._editProductDetailGroupBox.ResumeLayout(false);
            this._editProductDetailGroupBox.PerformLayout();
            this._categoryTabPage.ResumeLayout(false);
            this._tableLayoutPanel1.ResumeLayout(false);
            this._categoryGroupBox.ResumeLayout(false);
            this._tableLayoutPanel2.ResumeLayout(false);
            this._tableLayoutPanel3.ResumeLayout(false);
            this._tableLayoutPanel3.PerformLayout();
            this._categoryProductGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _mainTableLayoutPanel;
        private System.Windows.Forms.Label _productManagementSystemTitle;
        private System.Windows.Forms.TabControl _mainTabControl;
        private System.Windows.Forms.TabPage _productTabPage;
        private System.Windows.Forms.TableLayoutPanel _productManagementTableLayoutPanel;
        private System.Windows.Forms.Button _addNewProduct;
        private System.Windows.Forms.ListBox _productListListBox;
        private System.Windows.Forms.Button _productSaveDataButton;
        private System.Windows.Forms.TabPage _categoryTabPage;
        private System.Windows.Forms.GroupBox _editProductGroupBox;
        private System.Windows.Forms.TableLayoutPanel _editProductTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel _editProductListTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel _editProductPriceTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel _editProductCategoryTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel _editProductNameTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel _editProductPictureTableLayoutPanel;
        private System.Windows.Forms.GroupBox _editProductDetailGroupBox;
        private System.Windows.Forms.TextBox _editProductDetailTextBox;
        private System.Windows.Forms.Label _productPriceLabel;
        private System.Windows.Forms.Label _priceCounterLabel;
        private System.Windows.Forms.TextBox _productPriceTextBox;
        private System.Windows.Forms.Label _productCategoryLabel;
        private System.Windows.Forms.ComboBox _productCategoryComboBox;
        private System.Windows.Forms.Label _productNameLabel;
        private System.Windows.Forms.TextBox _productNameTextBox;
        private System.Windows.Forms.Label _productPicturePathLabel;
        private System.Windows.Forms.Button _browsePictureButton;
        private System.Windows.Forms.TextBox _productPicturePathTextBox;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.ListBox _productClassListBox;
        private System.Windows.Forms.GroupBox _categoryGroupBox;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.Label _classNameLabel;
        private System.Windows.Forms.TextBox _classNameTextBox;
        private System.Windows.Forms.Button _addProductClassButton;
        private System.Windows.Forms.GroupBox _categoryProductGroupBox;
        private System.Windows.Forms.ListBox _classProductListBox;
        private System.Windows.Forms.Button _saveClassButton;
    }
}
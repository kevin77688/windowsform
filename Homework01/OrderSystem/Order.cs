﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Drawing;
using System.ComponentModel;
using System.Linq;

namespace Homework01
{ 
    public partial class Order : Form
    {
        private string _currentDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
        private XmlDocument _productCatalogue = new XmlDocument();
        private int _totalPrice = 0;
        private int _currentIndex;
        private int _currentPages = 1;
        private BindingList<Goods> _allProduct;
        private BindingList<Goods> _currentProductList;
        private BindingList<Goods> _cart = new BindingList<Goods>();
        private BindingList<string> _tabName;
        private OrderPresentation _orderPresentation;
        private List<TabPage> _allTabPage = new List<TabPage>();
        private List<List<Button>> _allButton = new List<List<Button>>();

        public Order(OrderPresentation orderPresentation)
        {
            InitializeComponent();
            this._orderPresentation = orderPresentation;
            _productCatalogue.Load(_currentDirectory + Constant.DEFAULT_DATA_PATH);
            _allProduct = _orderPresentation.GetGoodsList();
            _tabName = _orderPresentation.GetGoodsClass();
            _tabName.ListChanged += UpdateCurrentTabPage;
            CreateTabPage();
            foreach (Goods goods in _allProduct)
                goods._cartOverload += CheckCartOverload;
            _allProduct.ListChanged += UpdateCurrentProductList;
            _currentProductList = new BindingList<Goods>(_allProduct.Where(product => product.GetClassName == _productTabControl.SelectedTab.Text).ToList());
            CreateButtonPicture();
            InitialProgram();
        }

        /// Initial all data from bindingList
        private void CreateTabPage()
        {
            _productTabControl.Controls.Clear();
            for (int index = 0; index < _tabName.Count; index++)
                _productTabControl.Controls.Add(CreateTabPage(_tabName[index], index));
        }

        /// Update current product list while _allproduct Changes
        private void UpdateCurrentProductList(object sender, ListChangedEventArgs e)
        {
            _currentProductList = new BindingList<Goods>(_allProduct.Where(product => product.GetClassName == _productTabControl.SelectedTab.Text).ToList());
            _currentPages = _orderPresentation.IsChangeToPreviousPages(_currentPages, _currentProductList);
            CreateButtonPicture();
            CheckNextButtonAndBackButton();
        }

        /// Re-create tab page while tabpage changes
        private void UpdateCurrentTabPage(object sender, ListChangedEventArgs e)
        {
            CreateTabPage();
            CreateButtonPicture();
            CheckNextButtonAndBackButton();
        }

        /// Initial DataGridView by databinging
        public void InitialDataGridView()
        {
            _dataGridView1.AutoGenerateColumns = _orderPresentation.IsAutoGenerateColumns();
            _dataGridView1.DataSource = _cart;
            _dataGridView1.Columns[Constant.NUMBER_ZERO].DataPropertyName = Constant.GET_DELETE_ICON;
            _dataGridView1.Columns[Constant.NUMBER_ONE].DataPropertyName = Constant.GET_NAME;
            _dataGridView1.Columns[Constant.NUMBER_TWO].DataPropertyName = Constant.GET_CLASS_NAME;
            _dataGridView1.Columns[Constant.NUMBER_THREE].DataPropertyName = Constant.GET_PRICE;
            _dataGridView1.Columns[Constant.NUMBER_FOUR].DataPropertyName = Constant.GET_LOCAL_QUANTITY;
            _dataGridView1.Columns[Constant.NUMBER_FIVE].DataPropertyName = Constant.GET_TOTAL_ROW_PRICE;
        }

        /// Dynamically create tabpage
        private TabPage CreateTabPage(string name, int tabIndex)
        {
            TabPage tab = new TabPage();
            tab.Text = name;
            tab.Controls.Add(CreateTablePanel());
            return tab;
        }

        /// Dynamically create tableLayoutPanel while Initialize
        private TableLayoutPanel CreateTablePanel()
        {
            TableLayoutPanel panel = new TableLayoutPanel();
            panel.ColumnCount = Constant.NUMBER_THREE;
            panel.RowCount = Constant.NUMBER_TWO;
            panel.Dock = DockStyle.Fill;
            List<Button> currentList = new List<Button>();
            for (int row = 0; row < panel.RowCount; row++)
            {
                panel.RowStyles.Add(new RowStyle(SizeType.Percent, 1F));
                for (int column = 0; column < panel.ColumnCount; column++)
                {
                    panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 1F));
                    panel.Controls.Add(CreateButton(), column, row);
                    currentList.Add((Button)panel.GetControlFromPosition(column, row));
                }
            }
            _allButton.Add(currentList);
            return panel;
        }

        /// Programatically create button and style
        private Button CreateButton()
        {
            Button button = new Button();
            button.Dock = DockStyle.Fill;
            button.Click += ClickButton;
            button.BackgroundImageLayout = ImageLayout.Stretch;
            return button;
        }

        /// Initial all file before the program
        private void InitialProgram()
        {
            _totalText.Text = _orderPresentation.TotalPriceString(_totalPrice);
            _switchPage.Text = _orderPresentation.SetPageString(_currentPages, _currentProductList);
            _backButton.Enabled = _orderPresentation.IsBackButtonEnable(_currentPages);
            _addButton.Enabled = _orderPresentation.IsAddButtonEnable();
            _payment.Enabled = _orderPresentation.IsPaymentButtonEnable(_totalPrice);
            SwitchButtonControl(false);
            File.Copy(_currentDirectory + Constant.DEFAULT_CREDIT_CARD_DATA, _currentDirectory + Constant.DEFAULT_TARGET_PATH, true);
            _quantity.Minimum = 1;
            _cart.ListChanged += CheckProductDataChange;
            InitialDataGridView();
        }

        /// Initial the button icon
        private void CreateButtonPicture()
        {
            if (_currentProductList.Count == 1)
                _currentPages = 1;
            for (int indexList = 0; indexList < _allButton.Count; indexList++)
                for (int indexButton = 0; indexButton < _allButton[indexList].Count; indexButton++)
                    if (indexButton + Constant.NUMBER_SIX * (_currentPages - 1) <= _currentProductList.Count - 1 && _currentProductList.Count > 0)
                    {
                        _allButton[indexList][indexButton].DataBindings.Clear();
                        _allButton[indexList][indexButton].Visible = true;
                        _allButton[indexList][indexButton].DataBindings.Add(Constant.BACKGROUND, _currentProductList[indexButton + Constant.NUMBER_SIX * (_currentPages - 1)], Constant.GET_PICTURE);
                    }
                    else
                        _allButton[indexList][indexButton].Visible = false;
        }

        ///change the global variable "_currentTabs
        private void ChangeTabIndex(object sender, EventArgs e)
        {
            TabControl tabControl = (TabControl)sender;
            TabPage tabPage = tabControl.SelectedTab;
            _currentPages = 1;
            if (tabPage != null)
                _currentProductList = new BindingList<Goods>(_allProduct.Where(product => product.GetClassName == tabPage.Text).ToList());
            CreateButtonPicture();
            CheckNextButtonAndBackButton();
            SwitchButtonControl(false);
        }

        /// Click button reaction
        private void ClickButton(object sender, EventArgs e)
        {
            Button currentButton = (Button)sender;
            _currentIndex = _orderPresentation.CountLocalIndex(_currentPages, currentButton.TabIndex);
            SetProductDetail();
        }

        /// Set the detail rich box text
        private void SetProductDetail()
        {
            SwitchButtonControl(true);
            _localQuantityValue.DataBindings.Clear();
            _addButton.DataBindings.Clear();
            _detailTitle.DataBindings.Clear();
            _detailText.DataBindings.Clear();
            _localPrice.DataBindings.Clear();
            _localQuantityValue.DataBindings.Clear();
            _detailTitle.DataBindings.Add(Constant.TEXT, _currentProductList[_currentIndex], Constant.GET_NAME);
            _detailText.DataBindings.Add(Constant.TEXT, _currentProductList[_currentIndex], Constant.GET_DETAIL);
            _localPrice.DataBindings.Add(Constant.TEXT, _currentProductList[_currentIndex], Constant.GET_PRODUCT_PRICE_STRING);
            _localQuantityValue.DataBindings.Add(Constant.TEXT, _currentProductList[_currentIndex], Constant.GET_QUANTITY);
            _addButton.DataBindings.Add(Constant.ENABLED, _currentProductList[_currentIndex], Constant.IS_BUTTON_ENABLE);
        }

        /// add button reation (add the item to the grid)
        private void AddButtonClick(object sender, EventArgs e)
        {
            if (_currentProductList[_currentIndex].GetInCartStatus == false)
            {
                SetDetail();
                CheckPaymentButton();
                _currentProductList[_currentIndex].GetInCartStatus = true;
            }
        }

        /// Set form detail
        private void SetDetail()
        {
            _cart.Add(_currentProductList[_currentIndex]);
            _cart.ResetBindings();
        }

        /// Check shopping cart overloaded or not
        private void CheckCartOverload(object sender, EventArgs e)
        {
            MessageBox.Show(Constant.QUANTITY_NOT_ENOUGH);
        }

        /// Payment Button Reaction
        private void ClickPayment(object sender, EventArgs e)
        {
            Payment paymentForm = new Payment(new PaymentPresentation(new PaymentModel()));
            paymentForm.ShowDialog();
            if (paymentForm.DialogResult.Equals(DialogResult.OK))
            {
                for (int row = 0; row < _dataGridView1.Rows.Count; row++)
                    _orderPresentation.ResetAllProductInCart(_cart);
                _cart.Clear();
                _dataGridView1.Rows.Clear();
                SwitchButtonControl(false);
                _totalPrice = 0;
                _totalText.Text = _orderPresentation.TotalPriceString(_totalPrice);
                _addButton.Enabled = false;
                CheckPaymentButton();
            }
        }

        /// Payment Disable or Enable
        private void CheckPaymentButton()
        {
            _payment.Enabled = _orderPresentation.IsPaymentButtonEnable(_totalPrice);
        }

        /// Deleted selected Rows
        private void DeleteSelectedRow(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                _orderPresentation.SetAddButtonEnable((string)_dataGridView1.Rows[e.RowIndex].Cells[Constant.NUMBER_ONE].Value);
                _cart.RemoveAt(e.RowIndex);
                CheckPaymentButton();
            }
        }

        /// Switch Pages action
        private void SwitchPages(object sender, EventArgs e)
        {
            Button pages = (Button)sender;
            _currentPages += Int32.Parse((string)pages.Tag);
            CreateButtonPicture();
            CheckNextButtonAndBackButton();
            SwitchButtonControl(false);
        }

        /// Change status while switching item
        private void SwitchButtonControl(bool set)
        {
            _detailTitle.Visible = set;
            _detailText.Visible = set;
            _localPrice.Visible = set;
            _localQuantityValue.Visible = set;
            _localQuantityTitle.Visible = set;
        }

        /// Check Next and Back Button action or not
        private void CheckNextButtonAndBackButton()
        {
            _nextButton.Enabled = _orderPresentation.IsNextButtonEnable(_currentProductList, _currentPages);
            _backButton.Enabled = _orderPresentation.IsBackButtonEnable(_currentPages);
            _switchPage.Text = _orderPresentation.SetPageString(_currentPages, _currentProductList);
        }

        /// Check product list while bindinglist changes
        private void CheckProductDataChange(object sender, ListChangedEventArgs e)
        {
            if (_dataGridView1.Rows.Count > 0 && _dataGridView1[Constant.NUMBER_FIVE, 0] != null)
                for (int index = 0; index < _dataGridView1.Rows.Count; index++)
                    _totalPrice = _orderPresentation.GetCartTotalPrice(_cart);
            _totalText.Text = String.Format(Constant.TOTAL_PRICE, _totalPrice);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace Homework01
{    
    public class OrderPresentation
    {
        private OrderModel _orderModel;

        public OrderPresentation(OrderModel orderModel)
        {
            this._orderModel = orderModel;
        }

        /// Find the page to string
        public string SetPageString(int currentPages, BindingList<Goods> currentData)
        {
            return _orderModel.SetPageString(currentPages, currentData);
        }

        /// Check Next Button enable
        public bool IsNextButtonEnable(BindingList<Goods> currentData, int currentPage)
        {
            return _orderModel.CheckOnePagesOverSix(currentData, currentPage);
        }

        /// Check Back Button
        public bool IsBackButtonEnable(int currentPages)
        {
            return _orderModel.IsBackButtonEnable(currentPages);
        }

        /// Initial add button
        public bool IsAddButtonEnable()
        {
            return false;
        }

        /// Count current Page number
        public int CountLocalIndex(int currentPage, int currentIndex)
        {
            return _orderModel.CountLocalIndex(currentPage, currentIndex);
        }

        /// Initial totalPrice text
        public string TotalPriceString(int totalPrice)
        {
            return String.Format(Constant.TOTAL_PRICE , totalPrice);
        }

        /// Check Payment Button
        public bool IsPaymentButtonEnable(int totalPrice)
        {
            return _orderModel.IsPaymentButtonEnable(totalPrice);
        }

        /// Set all product property "_isAddToCart" to false
        public void ResetAllProductInCart(BindingList<Goods> productList)
        {
            foreach (Goods good in productList)
            {
                good.GetInCartStatus = false;
                good.PurchaseProduct();
                good.GetLocalQuantity = 1;
            }
        }

        /// Return Recieved List
        public BindingList<Goods> GetGoodsList()
        {
            return _orderModel.GetGoodsList();
        }

        /// Return recieved class list
        public BindingList<string> GetGoodsClass()
        {
            return _orderModel.GetGoodsClass();
        }

        /// Get current rows total price
        public string GetRowsTotalPrice(string firstNumber, string secondNumber)
        {
            return String.Format(Constant.CHANGE_TO_CURRENCY, _orderModel.GetRowsTotalPrice(firstNumber, secondNumber));
        }

        /// Set add button to true;
        public void SetAddButtonEnable(string name)
        {
            _orderModel.SetAddButtonEnable(name);
        }

        /// Disable datagridview autogenerate columns
        public bool IsAutoGenerateColumns()
        {
            return false;
        }

        /// Count total price while bindinglist changes
        public int GetCartTotalPrice(BindingList<Goods> goods)
        {
            return _orderModel.GetCartTotalPrice(goods);
        }

        /// Check for changes to previous pages
        public int IsChangeToPreviousPages(int pages, BindingList<Goods> bindingList)
        {
            return _orderModel.IsChangeToPreviousPages(pages, bindingList);
        }
    }
}

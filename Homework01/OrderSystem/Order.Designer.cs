﻿namespace Homework01
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Order));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this._backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._groupBox1 = new System.Windows.Forms.GroupBox();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._goodsDetail = new System.Windows.Forms.GroupBox();
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._detailPriceTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._localPrice = new System.Windows.Forms.Label();
            this._quantityLabelTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._localQuantityTitle = new System.Windows.Forms.Label();
            this._localQuantityValue = new System.Windows.Forms.Label();
            this._tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._detailText = new System.Windows.Forms.RichTextBox();
            this._detailTitle = new System.Windows.Forms.Label();
            this._tableLayout2 = new System.Windows.Forms.TableLayoutPanel();
            this._addButton = new System.Windows.Forms.Button();
            this._backButton = new System.Windows.Forms.Button();
            this._nextButton = new System.Windows.Forms.Button();
            this._switchPage = new System.Windows.Forms.Label();
            this._productTabControl = new System.Windows.Forms.TabControl();
            this._tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this._dataGridView1 = new System.Windows.Forms.DataGridView();
            this._deleteRows = new System.Windows.Forms.DataGridViewImageColumn();
            this._goodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._goodsCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._goodsPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._quantity = new DataGridViewNumericUpDownElements.DataGridViewNumericUpDownColumn();
            this._currentRowTotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._title = new System.Windows.Forms.Label();
            this._tableLayout1 = new System.Windows.Forms.TableLayoutPanel();
            this._totalText = new System.Windows.Forms.Label();
            this._payment = new System.Windows.Forms.Button();
            this._tableLayoutPanel1.SuspendLayout();
            this._groupBox1.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this._goodsDetail.SuspendLayout();
            this._tableLayoutPanel3.SuspendLayout();
            this._detailPriceTableLayoutPanel.SuspendLayout();
            this._quantityLabelTableLayoutPanel.SuspendLayout();
            this._tableLayoutPanel.SuspendLayout();
            this._tableLayout2.SuspendLayout();
            this._tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView1)).BeginInit();
            this._tableLayout1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 2;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel1.Controls.Add(this._groupBox1, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel11, 1, 0);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 1;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 318F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(772, 318);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _groupBox1
            // 
            this._groupBox1.Controls.Add(this._tableLayoutPanel2);
            this._groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._groupBox1.Location = new System.Drawing.Point(2, 2);
            this._groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this._groupBox1.Name = "_groupBox1";
            this._groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this._groupBox1.Size = new System.Drawing.Size(382, 314);
            this._groupBox1.TabIndex = 0;
            this._groupBox1.TabStop = false;
            this._groupBox1.Text = "商品";
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 1;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Controls.Add(this._goodsDetail, 0, 1);
            this._tableLayoutPanel2.Controls.Add(this._tableLayout2, 0, 2);
            this._tableLayoutPanel2.Controls.Add(this._productTabControl, 0, 0);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(2, 21);
            this._tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 3;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(378, 291);
            this._tableLayoutPanel2.TabIndex = 0;
            // 
            // _goodsDetail
            // 
            this._goodsDetail.Controls.Add(this._tableLayoutPanel3);
            this._goodsDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this._goodsDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._goodsDetail.Location = new System.Drawing.Point(2, 176);
            this._goodsDetail.Margin = new System.Windows.Forms.Padding(2);
            this._goodsDetail.Name = "_goodsDetail";
            this._goodsDetail.Padding = new System.Windows.Forms.Padding(2);
            this._goodsDetail.Size = new System.Drawing.Size(374, 83);
            this._goodsDetail.TabIndex = 1;
            this._goodsDetail.TabStop = false;
            this._goodsDetail.Text = "商品介紹";
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 2;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel3.Controls.Add(this._detailPriceTableLayoutPanel, 1, 0);
            this._tableLayoutPanel3.Controls.Add(this._tableLayoutPanel, 0, 0);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(2, 16);
            this._tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 1;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(370, 65);
            this._tableLayoutPanel3.TabIndex = 0;
            // 
            // _detailPriceTableLayoutPanel
            // 
            this._detailPriceTableLayoutPanel.ColumnCount = 1;
            this._detailPriceTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._detailPriceTableLayoutPanel.Controls.Add(this._localPrice, 0, 1);
            this._detailPriceTableLayoutPanel.Controls.Add(this._quantityLabelTableLayoutPanel, 0, 0);
            this._detailPriceTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._detailPriceTableLayoutPanel.Location = new System.Drawing.Point(261, 2);
            this._detailPriceTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._detailPriceTableLayoutPanel.Name = "_detailPriceTableLayoutPanel";
            this._detailPriceTableLayoutPanel.RowCount = 2;
            this._detailPriceTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._detailPriceTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._detailPriceTableLayoutPanel.Size = new System.Drawing.Size(107, 61);
            this._detailPriceTableLayoutPanel.TabIndex = 1;
            // 
            // _localPrice
            // 
            this._localPrice.AutoSize = true;
            this._localPrice.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._localPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this._localPrice.Location = new System.Drawing.Point(2, 46);
            this._localPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._localPrice.Name = "_localPrice";
            this._localPrice.Size = new System.Drawing.Size(103, 15);
            this._localPrice.TabIndex = 3;
            // 
            // _quantityLabelTableLayoutPanel
            // 
            this._quantityLabelTableLayoutPanel.ColumnCount = 2;
            this._quantityLabelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._quantityLabelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._quantityLabelTableLayoutPanel.Controls.Add(this._localQuantityTitle, 0, 0);
            this._quantityLabelTableLayoutPanel.Controls.Add(this._localQuantityValue, 1, 0);
            this._quantityLabelTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._quantityLabelTableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this._quantityLabelTableLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this._quantityLabelTableLayoutPanel.Name = "_quantityLabelTableLayoutPanel";
            this._quantityLabelTableLayoutPanel.RowCount = 1;
            this._quantityLabelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._quantityLabelTableLayoutPanel.Size = new System.Drawing.Size(103, 26);
            this._quantityLabelTableLayoutPanel.TabIndex = 4;
            // 
            // _localQuantityTitle
            // 
            this._localQuantityTitle.AutoSize = true;
            this._localQuantityTitle.Dock = System.Windows.Forms.DockStyle.Right;
            this._localQuantityTitle.Location = new System.Drawing.Point(6, 0);
            this._localQuantityTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._localQuantityTitle.Name = "_localQuantityTitle";
            this._localQuantityTitle.Size = new System.Drawing.Size(64, 26);
            this._localQuantityTitle.TabIndex = 1;
            this._localQuantityTitle.Text = "庫存數量 : ";
            this._localQuantityTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _localQuantityValue
            // 
            this._localQuantityValue.AutoSize = true;
            this._localQuantityValue.Dock = System.Windows.Forms.DockStyle.Left;
            this._localQuantityValue.Location = new System.Drawing.Point(74, 0);
            this._localQuantityValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._localQuantityValue.Name = "_localQuantityValue";
            this._localQuantityValue.Size = new System.Drawing.Size(0, 26);
            this._localQuantityValue.TabIndex = 2;
            this._localQuantityValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _tableLayoutPanel
            // 
            this._tableLayoutPanel.ColumnCount = 1;
            this._tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel.Controls.Add(this._detailText, 0, 1);
            this._tableLayoutPanel.Controls.Add(this._detailTitle, 0, 0);
            this._tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this._tableLayoutPanel.Name = "_tableLayoutPanel";
            this._tableLayoutPanel.RowCount = 2;
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel.Size = new System.Drawing.Size(253, 59);
            this._tableLayoutPanel.TabIndex = 2;
            // 
            // _detailText
            // 
            this._detailText.BackColor = System.Drawing.SystemColors.Control;
            this._detailText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._detailText.Dock = System.Windows.Forms.DockStyle.Fill;
            this._detailText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._detailText.Location = new System.Drawing.Point(2, 32);
            this._detailText.Margin = new System.Windows.Forms.Padding(2);
            this._detailText.Name = "_detailText";
            this._detailText.Size = new System.Drawing.Size(249, 25);
            this._detailText.TabIndex = 1;
            this._detailText.Text = "";
            // 
            // _detailTitle
            // 
            this._detailTitle.AutoSize = true;
            this._detailTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._detailTitle.Location = new System.Drawing.Point(3, 0);
            this._detailTitle.Name = "_detailTitle";
            this._detailTitle.Size = new System.Drawing.Size(247, 30);
            this._detailTitle.TabIndex = 0;
            // 
            // _tableLayout2
            // 
            this._tableLayout2.ColumnCount = 4;
            this._tableLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this._tableLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayout2.Controls.Add(this._addButton, 3, 0);
            this._tableLayout2.Controls.Add(this._backButton, 1, 0);
            this._tableLayout2.Controls.Add(this._nextButton, 2, 0);
            this._tableLayout2.Controls.Add(this._switchPage, 0, 0);
            this._tableLayout2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayout2.Location = new System.Drawing.Point(2, 263);
            this._tableLayout2.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayout2.Name = "_tableLayout2";
            this._tableLayout2.RowCount = 1;
            this._tableLayout2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout2.Size = new System.Drawing.Size(374, 26);
            this._tableLayout2.TabIndex = 2;
            // 
            // _addButton
            // 
            this._addButton.AutoSize = true;
            this._addButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_addButton.BackgroundImage")));
            this._addButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._addButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addButton.Location = new System.Drawing.Point(299, 2);
            this._addButton.Margin = new System.Windows.Forms.Padding(2);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(73, 22);
            this._addButton.TabIndex = 0;
            this._addButton.UseVisualStyleBackColor = true;
            this._addButton.Click += new System.EventHandler(this.AddButtonClick);
            // 
            // _backButton
            // 
            this._backButton.AutoSize = true;
            this._backButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_backButton.BackgroundImage")));
            this._backButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._backButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this._backButton.Location = new System.Drawing.Point(152, 2);
            this._backButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._backButton.Name = "_backButton";
            this._backButton.Size = new System.Drawing.Size(68, 22);
            this._backButton.TabIndex = 1;
            this._backButton.Tag = "-1";
            this._backButton.UseVisualStyleBackColor = true;
            this._backButton.Click += new System.EventHandler(this.SwitchPages);
            // 
            // _nextButton
            // 
            this._nextButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_nextButton.BackgroundImage")));
            this._nextButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._nextButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._nextButton.Location = new System.Drawing.Point(226, 2);
            this._nextButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._nextButton.Name = "_nextButton";
            this._nextButton.Size = new System.Drawing.Size(68, 22);
            this._nextButton.TabIndex = 2;
            this._nextButton.Tag = "1";
            this._nextButton.UseVisualStyleBackColor = true;
            this._nextButton.Click += new System.EventHandler(this.SwitchPages);
            // 
            // _switchPage
            // 
            this._switchPage.AutoSize = true;
            this._switchPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this._switchPage.Location = new System.Drawing.Point(3, 2);
            this._switchPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._switchPage.Name = "_switchPage";
            this._switchPage.Size = new System.Drawing.Size(143, 22);
            this._switchPage.TabIndex = 3;
            // 
            // _productTabControl
            // 
            this._productTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this._productTabControl.Location = new System.Drawing.Point(2, 2);
            this._productTabControl.Margin = new System.Windows.Forms.Padding(2);
            this._productTabControl.Name = "_productTabControl";
            this._productTabControl.SelectedIndex = 0;
            this._productTabControl.Size = new System.Drawing.Size(374, 170);
            this._productTabControl.TabIndex = 3;
            this._productTabControl.SelectedIndexChanged += new System.EventHandler(this.ChangeTabIndex);
            // 
            // _tableLayoutPanel11
            // 
            this._tableLayoutPanel11.ColumnCount = 1;
            this._tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel11.Controls.Add(this._dataGridView1, 0, 1);
            this._tableLayoutPanel11.Controls.Add(this._title, 0, 0);
            this._tableLayoutPanel11.Controls.Add(this._tableLayout1, 0, 2);
            this._tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel11.Location = new System.Drawing.Point(388, 2);
            this._tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel11.Name = "_tableLayoutPanel11";
            this._tableLayoutPanel11.RowCount = 3;
            this._tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this._tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel11.Size = new System.Drawing.Size(382, 314);
            this._tableLayoutPanel11.TabIndex = 1;
            // 
            // _dataGridView1
            // 
            this._dataGridView1.AllowUserToAddRows = false;
            this._dataGridView1.AllowUserToDeleteRows = false;
            this._dataGridView1.AllowUserToResizeColumns = false;
            this._dataGridView1.AllowUserToResizeRows = false;
            this._dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._deleteRows,
            this._goodsName,
            this._goodsCategory,
            this._goodsPrice,
            this._quantity,
            this._currentRowTotalPrice});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this._dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridView1.Location = new System.Drawing.Point(2, 33);
            this._dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this._dataGridView1.Name = "_dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._dataGridView1.RowHeadersVisible = false;
            this._dataGridView1.RowTemplate.Height = 31;
            this._dataGridView1.Size = new System.Drawing.Size(378, 247);
            this._dataGridView1.TabIndex = 4;
            this._dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DeleteSelectedRow);
            // 
            // _deleteRows
            // 
            this._deleteRows.FillWeight = 64.97462F;
            this._deleteRows.HeaderText = "刪除";
            this._deleteRows.Name = "_deleteRows";
            this._deleteRows.ReadOnly = true;
            this._deleteRows.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _goodsName
            // 
            this._goodsName.FillWeight = 111.6751F;
            this._goodsName.HeaderText = "商品名稱";
            this._goodsName.Name = "_goodsName";
            // 
            // _goodsCategory
            // 
            this._goodsCategory.FillWeight = 111.6751F;
            this._goodsCategory.HeaderText = "商品類別";
            this._goodsCategory.Name = "_goodsCategory";
            // 
            // _goodsPrice
            // 
            this._goodsPrice.FillWeight = 111.6751F;
            this._goodsPrice.HeaderText = "單價";
            this._goodsPrice.Name = "_goodsPrice";
            // 
            // _quantity
            // 
            this._quantity.HeaderText = "數量";
            this._quantity.Name = "_quantity";
            this._quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _currentRowTotalPrice
            // 
            this._currentRowTotalPrice.HeaderText = "總價";
            this._currentRowTotalPrice.Name = "_currentRowTotalPrice";
            // 
            // _title
            // 
            this._title.AutoSize = true;
            this._title.Dock = System.Windows.Forms.DockStyle.Fill;
            this._title.Font = new System.Drawing.Font("PMingLiU", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._title.Location = new System.Drawing.Point(2, 0);
            this._title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._title.Name = "_title";
            this._title.Size = new System.Drawing.Size(378, 31);
            this._title.TabIndex = 0;
            this._title.Text = "我的訂單";
            this._title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _tableLayout1
            // 
            this._tableLayout1.ColumnCount = 2;
            this._tableLayout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this._tableLayout1.Controls.Add(this._totalText, 0, 0);
            this._tableLayout1.Controls.Add(this._payment, 1, 0);
            this._tableLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayout1.Location = new System.Drawing.Point(2, 284);
            this._tableLayout1.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayout1.Name = "_tableLayout1";
            this._tableLayout1.RowCount = 1;
            this._tableLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout1.Size = new System.Drawing.Size(378, 28);
            this._tableLayout1.TabIndex = 5;
            // 
            // _totalText
            // 
            this._totalText.AutoSize = true;
            this._totalText.Dock = System.Windows.Forms.DockStyle.Fill;
            this._totalText.Font = new System.Drawing.Font("PMingLiU", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._totalText.Location = new System.Drawing.Point(2, 0);
            this._totalText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._totalText.Name = "_totalText";
            this._totalText.Size = new System.Drawing.Size(274, 28);
            this._totalText.TabIndex = 0;
            this._totalText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _payment
            // 
            this._payment.Dock = System.Windows.Forms.DockStyle.Fill;
            this._payment.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this._payment.Location = new System.Drawing.Point(280, 2);
            this._payment.Margin = new System.Windows.Forms.Padding(2);
            this._payment.Name = "_payment";
            this._payment.Size = new System.Drawing.Size(96, 24);
            this._payment.TabIndex = 1;
            this._payment.Text = "訂購";
            this._payment.UseVisualStyleBackColor = true;
            this._payment.Click += new System.EventHandler(this.ClickPayment);
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 318);
            this.Controls.Add(this._tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Order";
            this.Text = "訂購";
            this._tableLayoutPanel1.ResumeLayout(false);
            this._groupBox1.ResumeLayout(false);
            this._tableLayoutPanel2.ResumeLayout(false);
            this._goodsDetail.ResumeLayout(false);
            this._tableLayoutPanel3.ResumeLayout(false);
            this._detailPriceTableLayoutPanel.ResumeLayout(false);
            this._detailPriceTableLayoutPanel.PerformLayout();
            this._quantityLabelTableLayoutPanel.ResumeLayout(false);
            this._quantityLabelTableLayoutPanel.PerformLayout();
            this._tableLayoutPanel.ResumeLayout(false);
            this._tableLayoutPanel.PerformLayout();
            this._tableLayout2.ResumeLayout(false);
            this._tableLayout2.PerformLayout();
            this._tableLayoutPanel11.ResumeLayout(false);
            this._tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView1)).EndInit();
            this._tableLayout1.ResumeLayout(false);
            this._tableLayout1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker _backgroundWorker1;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.GroupBox _groupBox1;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.GroupBox _goodsDetail;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel11;
        private System.Windows.Forms.Label _title;
        private System.Windows.Forms.TableLayoutPanel _tableLayout1;
        private System.Windows.Forms.Label _totalText;
        private System.Windows.Forms.DataGridView _dataGridView1;
        private System.Windows.Forms.TableLayoutPanel _tableLayout2;
        private System.Windows.Forms.Button _addButton;
        private System.Windows.Forms.Button _backButton;
        private System.Windows.Forms.Button _nextButton;
        private System.Windows.Forms.Label _switchPage;
        private System.Windows.Forms.Button _payment;
        private System.Windows.Forms.TableLayoutPanel _detailPriceTableLayoutPanel;
        private System.Windows.Forms.Label _localPrice;
        private System.Windows.Forms.TableLayoutPanel _quantityLabelTableLayoutPanel;
        private System.Windows.Forms.Label _localQuantityTitle;
        private System.Windows.Forms.Label _localQuantityValue;
        private System.Windows.Forms.DataGridViewImageColumn _deleteRows;
        private System.Windows.Forms.DataGridViewTextBoxColumn _goodsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn _goodsCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn _goodsPrice;
        private DataGridViewNumericUpDownElements.DataGridViewNumericUpDownColumn _quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn _currentRowTotalPrice;
        private System.Windows.Forms.TabControl _productTabControl;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel;
        private System.Windows.Forms.RichTextBox _detailText;
        private System.Windows.Forms.Label _detailTitle;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace Homework01
{
    public class OrderModel
    {
        readonly private BindingList<Goods> _allProduct;
        readonly private BindingList<string> _allClassName;
        public OrderModel(BindingList<Goods> productList, BindingList<string> productClass)
        {
            _allProduct = productList;
            _allClassName = productClass;
        }

        /// Check pages over six pages
        public bool CheckOnePagesOverSix(BindingList<Goods> currentData, int currentPage)
        {
            return (currentData.Count > currentPage * Constant.NUMBER_SIX) ? true : false;
        }

        /// Check Back Pages enable
        public bool IsBackButtonEnable(int currentPages)
        {
            return currentPages > Constant.NUMBER_ONE;
        }

        /// Find the page to string
        public string SetPageString(int currentPages, BindingList<Goods> currentData)
        {
            if (currentPages > (int)Math.Ceiling((double)currentData.Count / Constant.NUMBER_SIX))
                return (Constant.PAGES + 0.ToString() + Constant.SLASH + 0.ToString());
            return (Constant.PAGES + currentPages + Constant.SLASH + (int)Math.Ceiling((double)currentData.Count / Constant.NUMBER_SIX));
        }

        /// Check payment Enable by price
        public bool IsPaymentButtonEnable(int totalPrice)
        {
            return (totalPrice <= 0) ? false : true;
        }

        /// Count current pages method
        public int CountLocalIndex(int currentPage, int currentIndex)
        {
            if (currentPage < 1)
                throw new ArgumentException(Constant.PAGE_BELOW_ONE);
            return (currentPage - 1) * Constant.NUMBER_SIX + currentIndex;
        }

        /// Return Recieved List
        public BindingList<Goods> GetGoodsList()
        {
            return _allProduct;
        }

        /// Return recieved class list
        public BindingList<string> GetGoodsClass()
        {
            return _allClassName;
        }

        /// Get current rows total price
        public int GetRowsTotalPrice(string firstNumber, string secondNumber)
        {
            return (int.Parse(firstNumber) * int.Parse(secondNumber));
        }

        /// Set add button to true;
        public void SetAddButtonEnable(string name)
        {
            foreach (Goods product in _allProduct)
                if (product.GetName == name)
                    product.GetInCartStatus = false;
        }

        /// Count total price while bindinglist changes
        public int GetCartTotalPrice(BindingList<Goods> goods)
        {
            int totalPrice = 0;
            foreach (Goods product in goods)
                totalPrice += product.GetTotalRowPriceNumber;
            return totalPrice;
        }

        /// Check for changes to previous pages
        public int IsChangeToPreviousPages(int pages, BindingList<Goods> bindingList)
        {
            if (bindingList.Count - Constant.NUMBER_SIX * (pages - 1) > 0)
                return pages;
            else
                return pages -= 1;
        }
    }
}

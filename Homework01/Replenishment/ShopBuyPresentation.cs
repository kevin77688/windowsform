﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework01
{
    public class ShopBuyPresentation
    {
        private ShopBuyModel _shopBuyModel;
        public ShopBuyPresentation(ShopBuyModel model)
        {
            _shopBuyModel = model;
        }

        /// Get row product name
        public string GetName()
        {
            return _shopBuyModel.GetName();
        }

        /// Get row product class name
        public string GetClass()
        {
            return _shopBuyModel.GetClass();
        }

        /// Get row product price
        public int GetPrice()
        {
            return _shopBuyModel.GetPrice();
        }

        /// Get row product quantity
        public int GetQuantity()
        {
            return _shopBuyModel.GetQuantity();
        }

        /// Add quantity to class
        public void SetQuantity(int number)
        {
            _shopBuyModel.SetQuantity(number);
        }
    }
}

﻿namespace Homework01
{
    partial class ShopBuy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._basicLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._title = new System.Windows.Forms.Label();
            this._detailLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._productNameTitle = new System.Windows.Forms.Label();
            this._productClassTitle = new System.Windows.Forms.Label();
            this._productPriceTitle = new System.Windows.Forms.Label();
            this._productQuantityLeftTitle = new System.Windows.Forms.Label();
            this._buyQuantityTitle = new System.Windows.Forms.Label();
            this._productTitle = new System.Windows.Forms.Label();
            this._productClassName = new System.Windows.Forms.Label();
            this._productPrice = new System.Windows.Forms.Label();
            this._productQuantityLeft = new System.Windows.Forms.Label();
            this._buyQuantity = new System.Windows.Forms.NumericUpDown();
            this._buttonLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._acceptButton = new System.Windows.Forms.Button();
            this._declineButton = new System.Windows.Forms.Button();
            this._basicLayoutPanel.SuspendLayout();
            this._detailLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._buyQuantity)).BeginInit();
            this._buttonLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _basicLayoutPanel
            // 
            this._basicLayoutPanel.ColumnCount = 1;
            this._basicLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._basicLayoutPanel.Controls.Add(this._title, 0, 0);
            this._basicLayoutPanel.Controls.Add(this._detailLayoutPanel, 0, 1);
            this._basicLayoutPanel.Controls.Add(this._buttonLayoutPanel, 0, 2);
            this._basicLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._basicLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._basicLayoutPanel.Name = "_basicLayoutPanel";
            this._basicLayoutPanel.RowCount = 3;
            this._basicLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this._basicLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 71.42653F));
            this._basicLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28816F));
            this._basicLayoutPanel.Size = new System.Drawing.Size(452, 421);
            this._basicLayoutPanel.TabIndex = 0;
            // 
            // _title
            // 
            this._title.AutoSize = true;
            this._title.Dock = System.Windows.Forms.DockStyle.Fill;
            this._title.Font = new System.Drawing.Font("Microsoft YaHei", 22F);
            this._title.Location = new System.Drawing.Point(3, 0);
            this._title.Name = "_title";
            this._title.Size = new System.Drawing.Size(446, 60);
            this._title.TabIndex = 0;
            this._title.Text = "補貨單";
            this._title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _detailLayoutPanel
            // 
            this._detailLayoutPanel.ColumnCount = 2;
            this._detailLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.84211F));
            this._detailLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.15789F));
            this._detailLayoutPanel.Controls.Add(this._productNameTitle, 0, 0);
            this._detailLayoutPanel.Controls.Add(this._productClassTitle, 0, 1);
            this._detailLayoutPanel.Controls.Add(this._productPriceTitle, 0, 2);
            this._detailLayoutPanel.Controls.Add(this._productQuantityLeftTitle, 0, 3);
            this._detailLayoutPanel.Controls.Add(this._buyQuantityTitle, 0, 4);
            this._detailLayoutPanel.Controls.Add(this._productTitle, 1, 0);
            this._detailLayoutPanel.Controls.Add(this._productClassName, 1, 1);
            this._detailLayoutPanel.Controls.Add(this._productPrice, 1, 2);
            this._detailLayoutPanel.Controls.Add(this._productQuantityLeft, 1, 3);
            this._detailLayoutPanel.Controls.Add(this._buyQuantity, 1, 4);
            this._detailLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._detailLayoutPanel.Location = new System.Drawing.Point(3, 63);
            this._detailLayoutPanel.Name = "_detailLayoutPanel";
            this._detailLayoutPanel.RowCount = 5;
            this._detailLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.00001F));
            this._detailLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._detailLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._detailLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._detailLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._detailLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._detailLayoutPanel.Size = new System.Drawing.Size(446, 294);
            this._detailLayoutPanel.TabIndex = 1;
            // 
            // _productNameTitle
            // 
            this._productNameTitle.AutoSize = true;
            this._productNameTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productNameTitle.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productNameTitle.Location = new System.Drawing.Point(3, 0);
            this._productNameTitle.Name = "_productNameTitle";
            this._productNameTitle.Size = new System.Drawing.Size(158, 58);
            this._productNameTitle.TabIndex = 0;
            this._productNameTitle.Text = "商品名稱 : ";
            this._productNameTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _productClassTitle
            // 
            this._productClassTitle.AutoSize = true;
            this._productClassTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productClassTitle.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productClassTitle.Location = new System.Drawing.Point(3, 58);
            this._productClassTitle.Name = "_productClassTitle";
            this._productClassTitle.Size = new System.Drawing.Size(158, 58);
            this._productClassTitle.TabIndex = 1;
            this._productClassTitle.Text = "商品類別 : ";
            this._productClassTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _productPriceTitle
            // 
            this._productPriceTitle.AutoSize = true;
            this._productPriceTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productPriceTitle.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productPriceTitle.Location = new System.Drawing.Point(3, 116);
            this._productPriceTitle.Name = "_productPriceTitle";
            this._productPriceTitle.Size = new System.Drawing.Size(158, 58);
            this._productPriceTitle.TabIndex = 2;
            this._productPriceTitle.Text = "商品單價 : ";
            this._productPriceTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _productQuantityLeftTitle
            // 
            this._productQuantityLeftTitle.AutoSize = true;
            this._productQuantityLeftTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productQuantityLeftTitle.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productQuantityLeftTitle.Location = new System.Drawing.Point(3, 174);
            this._productQuantityLeftTitle.Name = "_productQuantityLeftTitle";
            this._productQuantityLeftTitle.Size = new System.Drawing.Size(158, 58);
            this._productQuantityLeftTitle.TabIndex = 3;
            this._productQuantityLeftTitle.Text = "庫存數量 : ";
            this._productQuantityLeftTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _buyQuantityTitle
            // 
            this._buyQuantityTitle.AutoSize = true;
            this._buyQuantityTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._buyQuantityTitle.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._buyQuantityTitle.Location = new System.Drawing.Point(3, 232);
            this._buyQuantityTitle.Name = "_buyQuantityTitle";
            this._buyQuantityTitle.Size = new System.Drawing.Size(158, 62);
            this._buyQuantityTitle.TabIndex = 4;
            this._buyQuantityTitle.Text = "補貨數量 : ";
            this._buyQuantityTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _productTitle
            // 
            this._productTitle.AutoSize = true;
            this._productTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productTitle.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productTitle.Location = new System.Drawing.Point(167, 0);
            this._productTitle.Name = "_productTitle";
            this._productTitle.Size = new System.Drawing.Size(276, 58);
            this._productTitle.TabIndex = 5;
            this._productTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _productClassName
            // 
            this._productClassName.AutoSize = true;
            this._productClassName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productClassName.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productClassName.Location = new System.Drawing.Point(167, 58);
            this._productClassName.Name = "_productClassName";
            this._productClassName.Size = new System.Drawing.Size(276, 58);
            this._productClassName.TabIndex = 6;
            this._productClassName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _productPrice
            // 
            this._productPrice.AutoSize = true;
            this._productPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productPrice.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productPrice.Location = new System.Drawing.Point(167, 116);
            this._productPrice.Name = "_productPrice";
            this._productPrice.Size = new System.Drawing.Size(276, 58);
            this._productPrice.TabIndex = 7;
            this._productPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _productQuantityLeft
            // 
            this._productQuantityLeft.AutoSize = true;
            this._productQuantityLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productQuantityLeft.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._productQuantityLeft.Location = new System.Drawing.Point(167, 174);
            this._productQuantityLeft.Name = "_productQuantityLeft";
            this._productQuantityLeft.Size = new System.Drawing.Size(276, 58);
            this._productQuantityLeft.TabIndex = 8;
            this._productQuantityLeft.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _buyQuantity
            // 
            this._buyQuantity.Dock = System.Windows.Forms.DockStyle.Fill;
            this._buyQuantity.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._buyQuantity.Location = new System.Drawing.Point(167, 244);
            this._buyQuantity.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this._buyQuantity.Name = "_buyQuantity";
            this._buyQuantity.Size = new System.Drawing.Size(276, 36);
            this._buyQuantity.TabIndex = 9;
            // 
            // _buttonLayoutPanel
            // 
            this._buttonLayoutPanel.ColumnCount = 2;
            this._buttonLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._buttonLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._buttonLayoutPanel.Controls.Add(this._acceptButton, 0, 0);
            this._buttonLayoutPanel.Controls.Add(this._declineButton, 1, 0);
            this._buttonLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._buttonLayoutPanel.Location = new System.Drawing.Point(3, 363);
            this._buttonLayoutPanel.Name = "_buttonLayoutPanel";
            this._buttonLayoutPanel.RowCount = 1;
            this._buttonLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._buttonLayoutPanel.Size = new System.Drawing.Size(446, 55);
            this._buttonLayoutPanel.TabIndex = 2;
            // 
            // _acceptButton
            // 
            this._acceptButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this._acceptButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._acceptButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._acceptButton.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._acceptButton.Location = new System.Drawing.Point(3, 10);
            this._acceptButton.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this._acceptButton.Name = "_acceptButton";
            this._acceptButton.Size = new System.Drawing.Size(217, 42);
            this._acceptButton.TabIndex = 0;
            this._acceptButton.Text = "確認";
            this._acceptButton.UseVisualStyleBackColor = false;
            this._acceptButton.Click += new System.EventHandler(this.AcceptButtonTrigger);
            // 
            // _declineButton
            // 
            this._declineButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this._declineButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._declineButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._declineButton.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._declineButton.Location = new System.Drawing.Point(226, 10);
            this._declineButton.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this._declineButton.Name = "_declineButton";
            this._declineButton.Size = new System.Drawing.Size(217, 42);
            this._declineButton.TabIndex = 1;
            this._declineButton.Text = "取消";
            this._declineButton.UseVisualStyleBackColor = false;
            this._declineButton.Click += new System.EventHandler(this.CancelButtonTrigger);
            // 
            // Replenishment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 421);
            this.Controls.Add(this._basicLayoutPanel);
            this.Name = "Replenishment";
            this.Text = "Replenishment";
            this._basicLayoutPanel.ResumeLayout(false);
            this._basicLayoutPanel.PerformLayout();
            this._detailLayoutPanel.ResumeLayout(false);
            this._detailLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._buyQuantity)).EndInit();
            this._buttonLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _basicLayoutPanel;
        private System.Windows.Forms.Label _title;
        private System.Windows.Forms.TableLayoutPanel _detailLayoutPanel;
        private System.Windows.Forms.Label _productNameTitle;
        private System.Windows.Forms.Label _productClassTitle;
        private System.Windows.Forms.Label _productPriceTitle;
        private System.Windows.Forms.Label _productQuantityLeftTitle;
        private System.Windows.Forms.Label _buyQuantityTitle;
        private System.Windows.Forms.Label _productTitle;
        private System.Windows.Forms.Label _productClassName;
        private System.Windows.Forms.Label _productPrice;
        private System.Windows.Forms.Label _productQuantityLeft;
        private System.Windows.Forms.NumericUpDown _buyQuantity;
        private System.Windows.Forms.TableLayoutPanel _buttonLayoutPanel;
        private System.Windows.Forms.Button _acceptButton;
        private System.Windows.Forms.Button _declineButton;
    }
}
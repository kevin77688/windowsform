﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01
{
    public class ShopBuyModel
    {
        readonly private string _productName;
        readonly private string _productClass;
        readonly private int _productPrice;
        readonly private int _productQuantity;
        private Goods _currentProduct;

        public ShopBuyModel(List<string> rowData, BindingList<Goods> allProduct)
        {
            _productName = rowData[Constant.NUMBER_ZERO];
            _productClass = rowData[Constant.NUMBER_ONE];
            _productPrice = int.Parse(rowData[Constant.NUMBER_TWO]);
            _productQuantity = int.Parse(rowData[Constant.NUMBER_THREE]);
            foreach (Goods product in allProduct)
                if (product.GetName == _productName)
                    _currentProduct = product;
        }

        /// Get row product name
        public string GetName()
        {
            return _productName;
        }

        /// Get row product class name
        public string GetClass()
        {
            return _productClass;
        }

        /// Get row product price
        public int GetPrice()
        {
            return _productPrice;
        }

        /// Get row product quantity
        public int GetQuantity()
        {
            return _productQuantity;
        }

        /// Add quantity to class
        public void SetQuantity(int number)
        {
            _currentProduct.GetQuantity += number;
        }
    }
}

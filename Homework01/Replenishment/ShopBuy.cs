﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Homework01
{
    public partial class ShopBuy : Form
    {
        private ShopBuyPresentation _shopBuyPresentation;
        public ShopBuy(ShopBuyPresentation presentation)
        {
            InitializeComponent();
            _shopBuyPresentation = presentation;
            _productTitle.Text = _shopBuyPresentation.GetName();
            _productClassName.Text = _shopBuyPresentation.GetClass();
            _productPrice.Text = _shopBuyPresentation.GetPrice().ToString();
            _productQuantityLeft.Text = _shopBuyPresentation.GetQuantity().ToString();
        }

        /// Action while press accept button
        private void AcceptButtonTrigger(object sender, EventArgs e)
        {
            _shopBuyPresentation.SetQuantity(int.Parse(_buyQuantity.Value.ToString()));
            this.Close();
        }

        /// Action while press decline button
        private void CancelButtonTrigger(object sender , EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;

namespace Homework01
{
    public partial class Menu : Form
    {
        private MenuPresentation _menuPresentation;
        private BindingList<Goods> _goodsList;
        private BindingList<string> _goodsClass;
        private Order _orderForm;
        private InventorySystem _inventorySystem;
        private ProductManagementSystem _productManagementSystem;
        public Menu(MenuPresentation menuPresentation)
        {
            InitializeComponent();
            this._menuPresentation = menuPresentation;
            _goodsList = _menuPresentation.GetGoodsList();
            _goodsClass = _menuPresentation.GetGoodsClass();
            _orderForm = new Order(new OrderPresentation(new OrderModel(_goodsList, _goodsClass)));
            _orderForm.FormClosing += CheckOrderFormClosed;
            _inventorySystem = new InventorySystem(new InventoryPresentation(new InventoryModel(_goodsList)));
            _inventorySystem.FormClosing += CheckInventorySystemClosed;
            _productManagementSystem = new ProductManagementSystem(new ProductManagementPresentation(new ProductManagementModel(_goodsList, _goodsClass)));
            _productManagementSystem.FormClosing += CheckProductManagementSystemClosed;
        }

        /// Show order system dialog button
        private void ClickOrderSystemButton(object sender, EventArgs e)
        {
            _orderSystemButton.Enabled = _menuPresentation.IsOrderSystemEnable();
            _orderForm.Show();
        }

        /// Inventory system dialog
        private void ClickInventorySystemButton(object sender, EventArgs e)
        {
            _inventorySystemButton.Enabled = _menuPresentation.IsInventorySystemEnable();
            _inventorySystem.Show();
        }

        /// Show Product Management System dialog button
        private void ClickProductManagementSystemButton(object sender, EventArgs e)
        {
            _productManagementSystemButton.Enabled = _menuPresentation.IsProductManagementSystemEnable();
            _productManagementSystem.Show();
        }

        /// Check order form and do reaction
        private void CheckOrderFormClosed(Object objectItem, FormClosingEventArgs close)
        {
            _orderSystemButton.Enabled = _menuPresentation.IsOrderSystemClosed();
            File.Delete(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + Constant.DEFAULT_TARGET_PATH);
            if (close.CloseReason == CloseReason.UserClosing)
            {
                close.Cancel = true;
                _orderForm.Hide();
            }
        }

        /// Check Inventory system form and do reaction
        private void CheckInventorySystemClosed(object objectItem, FormClosingEventArgs close)
        {
            _inventorySystemButton.Enabled = _menuPresentation.IsInventorySystemClosed();
            if (close.CloseReason == CloseReason.UserClosing)
            {
                close.Cancel = true;
                _inventorySystem.Hide();
            }
        }

        /// Check Inventory system form and do reaction
        private void CheckProductManagementSystemClosed(object objectItem, FormClosingEventArgs close)
        {
            _productManagementSystemButton.Enabled = _menuPresentation.IsProductManagementSystemClosed();
            if (close.CloseReason == CloseReason.UserClosing)
            {
                close.Cancel = true;
                _productManagementSystem.Hide();
            }
        }

        /// Exit the full program
        private void ExitProgram(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

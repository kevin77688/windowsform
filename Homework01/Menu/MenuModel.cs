﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.ComponentModel;
using System.Drawing;

namespace Homework01
{
    public class MenuModel
    {
        readonly private string _currentDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
        private XmlDocument _productCatalogue = new XmlDocument();
        private BindingList<Goods> _allProduct = new BindingList<Goods>();
        private readonly BindingList<string> _productClass = new BindingList<string>();

        public MenuModel()
        {
            _productCatalogue.Load(_currentDirectory + Constant.DEFAULT_DATA_PATH);
            XmlNodeList currentAllNodes = _productCatalogue.SelectNodes(Constant.DEFAULT_PRODUCT_PATH);
            foreach (XmlNode currentNode in currentAllNodes)
            {
                string category = currentNode.Attributes[Constant.CATEGORY].Value;
                if (_productClass.Contains(category) == false)
                    _productClass.Add(category);
                string name = currentNode.ChildNodes[0].InnerText;
                string detail = currentNode.ChildNodes[1].InnerText;
                int price = int.Parse(currentNode.ChildNodes[Constant.NUMBER_TWO].InnerText);
                int quantity = int.Parse(currentNode.ChildNodes[Constant.NUMBER_THREE].InnerText);
                string picture = @currentNode.ChildNodes[Constant.NUMBER_FOUR].InnerText;
                _allProduct.Add(new Goods(category, name, detail, price, picture, quantity));
            }
        }

        /// Return created class list
        public BindingList<Goods> GetGoodsList()
        {
            return _allProduct;
        }

        /// Return created class
        public BindingList<string> GetGoodsClass()
        {
            return _productClass;
        }
    }
}

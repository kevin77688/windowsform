﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01
{
    public class MenuPresentation
    {
        readonly private bool _orderSystemStart = false;
        readonly private bool _inventorySystemStart = false;
        readonly private bool _productManagementSystemStart = false;
        readonly private bool _orderSystemClosed = true;
        readonly private bool _inventorySystemClosed = true;
        readonly private bool _productManagementSystemClosed = true;
        private MenuModel _menuModel;

        public MenuPresentation(MenuModel menuModel)
        {
            this._menuModel = menuModel;
        }

        /// return order system start status
        public bool IsOrderSystemEnable()
        {
            return _orderSystemStart;
        }

        /// return order system close status
        public bool IsOrderSystemClosed()
        {
            return _orderSystemClosed;
        }

        /// return inventory system start status
        public bool IsInventorySystemEnable()
        {
            return _inventorySystemStart;
        }

        /// return inventory system closed status
        public bool IsInventorySystemClosed()
        {
            return _inventorySystemClosed;
        }

        /// return product management system start status
        public bool IsProductManagementSystemEnable()
        {
            return _productManagementSystemStart;
        }

        /// return product management system closed status
        public bool IsProductManagementSystemClosed()
        {
            return _productManagementSystemClosed;
        }

        /// Return created class list
        public BindingList<Goods> GetGoodsList()
        {
            return _menuModel.GetGoodsList();
        }

        /// Return created class
        public BindingList<string> GetGoodsClass()
        {
            return _menuModel.GetGoodsClass();
        }
    }
}

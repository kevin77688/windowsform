﻿namespace Homework01
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._exit = new System.Windows.Forms.Button();
            this._orderSystemButton = new System.Windows.Forms.Button();
            this._inventorySystemButton = new System.Windows.Forms.Button();
            this._productManagementSystemButton = new System.Windows.Forms.Button();
            this._tableLayoutPanel1.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 1;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.Controls.Add(this._orderSystemButton, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._inventorySystemButton, 0, 1);
            this._tableLayoutPanel1.Controls.Add(this._productManagementSystemButton, 0, 2);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel2, 0, 3);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 4;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(782, 400);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 2;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel2.Controls.Add(this._exit, 1, 0);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(3, 302);
            this._tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 1;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(776, 96);
            this._tableLayoutPanel2.TabIndex = 3;
            // 
            // _exit
            // 
            this._exit.AutoSize = true;
            this._exit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._exit.Font = new System.Drawing.Font("PMingLiU", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._exit.Location = new System.Drawing.Point(552, 8);
            this._exit.Margin = new System.Windows.Forms.Padding(9, 8, 9, 8);
            this._exit.Name = "_exit";
            this._exit.Size = new System.Drawing.Size(215, 80);
            this._exit.TabIndex = 0;
            this._exit.Text = "Exit";
            this._exit.UseVisualStyleBackColor = true;
            this._exit.Click += new System.EventHandler(this.ExitProgram);
            // 
            // _orderSystemButton
            // 
            this._orderSystemButton.AutoSize = true;
            this._orderSystemButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._orderSystemButton.Font = new System.Drawing.Font("PMingLiU", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._orderSystemButton.Location = new System.Drawing.Point(9, 8);
            this._orderSystemButton.Margin = new System.Windows.Forms.Padding(9, 8, 9, 8);
            this._orderSystemButton.Name = "_orderSystemButton";
            this._orderSystemButton.Size = new System.Drawing.Size(764, 84);
            this._orderSystemButton.TabIndex = 0;
            this._orderSystemButton.Text = "Order System";
            this._orderSystemButton.UseVisualStyleBackColor = true;
            this._orderSystemButton.Click += new System.EventHandler(this.ClickOrderSystemButton);
            // 
            // _inventorySystemButton
            // 
            this._inventorySystemButton.AutoSize = true;
            this._inventorySystemButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inventorySystemButton.Font = new System.Drawing.Font("PMingLiU", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._inventorySystemButton.Location = new System.Drawing.Point(9, 108);
            this._inventorySystemButton.Margin = new System.Windows.Forms.Padding(9, 8, 9, 8);
            this._inventorySystemButton.Name = "_inventorySystemButton";
            this._inventorySystemButton.Size = new System.Drawing.Size(764, 84);
            this._inventorySystemButton.TabIndex = 1;
            this._inventorySystemButton.Text = "Inventory System";
            this._inventorySystemButton.UseVisualStyleBackColor = true;
            this._inventorySystemButton.Click += new System.EventHandler(this.ClickInventorySystemButton);
            // 
            // _productManagementSystemButton
            // 
            this._productManagementSystemButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productManagementSystemButton.Font = new System.Drawing.Font("PMingLiU", 26F);
            this._productManagementSystemButton.Location = new System.Drawing.Point(9, 208);
            this._productManagementSystemButton.Margin = new System.Windows.Forms.Padding(9, 8, 9, 8);
            this._productManagementSystemButton.Name = "_productManagementSystemButton";
            this._productManagementSystemButton.Size = new System.Drawing.Size(764, 84);
            this._productManagementSystemButton.TabIndex = 2;
            this._productManagementSystemButton.Text = "Product Management Form";
            this._productManagementSystemButton.UseVisualStyleBackColor = true;
            this._productManagementSystemButton.Click += new System.EventHandler(this.ClickProductManagementSystemButton);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 400);
            this.Controls.Add(this._tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Menu";
            this.Text = "Menu";
            this._tableLayoutPanel1.ResumeLayout(false);
            this._tableLayoutPanel1.PerformLayout();
            this._tableLayoutPanel2.ResumeLayout(false);
            this._tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.Button _orderSystemButton;
        private System.Windows.Forms.Button _inventorySystemButton;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.Button _exit;
        private System.Windows.Forms.Button _productManagementSystemButton;
    }
}
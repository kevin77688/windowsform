﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.ComponentModel;

namespace Homework01
{
    public class Goods : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler _cartOverload;
        private string _name;
        private string _detail;
        private string _className;
        private int _price;
        private System.Drawing.Bitmap _picture;
        private string _picturePath;
        private int _quantity;
        private int _localQuantity = 1;
        private bool _isAddToCart;
        private bool _isButtonEnable = true;

        /// Data Binding Notify function
        private void Notify(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// If selected quantity exceed total quantity, raise an event
        protected virtual void CartOverloadEvent(EventArgs e)
        {
            if (_cartOverload != null)
                _cartOverload(this, e);
        }

        public Goods(string className, string name, string detail, int price, string picture, int quantity, bool isAddToCart = false)
        {
            _className = className;
            _name = name;
            _detail = detail;
            _price = price;
            _picture = new Bitmap(Image.FromFile(picture));
            _picturePath = picture;
            _quantity = quantity;
            _isAddToCart = isAddToCart;
        }

        /// Return class name
        public string GetClassName
        {
            get
            {
                return _className;
            }
            set
            {
                _className = value;
                Notify(Constant.GET_CLASS_NAME);
            }
        }

        /// Return MotherBoard Name
        public string GetName
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                Notify(Constant.GET_NAME);
            }
        }

        /// Return MotherBoard detaiil
        public string GetDetail
        {
            get
            {
                return _detail;
            }
            set
            {
                _detail = value;
                Notify(Constant.GET_DETAIL);
            }
        }

        /// Return MotherBoard Price
        public int GetPrice
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                Notify(Constant.GET_PRICE);
                Notify(Constant.GET_TOTAL_ROW_PRICE);
                Notify(Constant.GET_PRODUCT_PRICE_STRING);
                Notify(Constant.GET_TOTAL_ROW_PRICE_NUMBER);
            }
        }

        public string GetProductPriceString
        {
            get
            {
                return String.Format(Constant.PRICE_NAME, _price);
            }
        }

        /// Get MotherBoard Picture
        public System.Drawing.Bitmap GetPicture
        {
            get
            {
                return _picture;
            }
            set
            {
                _picture = value;
                Notify(Constant.GET_PICTURE);
            }
        }

        public string GetPicturePath
        {
            get
            {
                return _picturePath;
            }
            set
            {
                _picturePath = value;
                Notify(Constant.GET_PICTURE_PATH);
            }
        }

        public int GetQuantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value != this._quantity)
                {
                    _quantity = value;
                    Notify(Constant.GET_QUANTITY);
                    Notify(Constant.IS_BUTTON_ENABLE);
                }
            }
        }

        public Image GetBuyImage
        {
            get
            {
                return Image.FromFile(Constant.DEFAULT_BUY_IMAGE);
            }
        }

        public Image GetDeleteImage
        {
            get
            {
                return Image.FromFile(Constant.DEFAULT_TRASH_ICON_PATH);
            }
        }

        public bool GetInCartStatus
        {
            get
            {
                return _isAddToCart;
            }
            set
            {
                _isAddToCart = value;
                Notify(Constant.GET_IN_CART_STATUS);
                Notify(Constant.IS_BUTTON_ENABLE);
            }
        }

        /// Action while purchase item
        public void PurchaseProduct()
        {
            GetQuantity -= _localQuantity;
            _localQuantity = 1;
        }

        public bool IsButtonEnable
        {
            get
            {
                _isButtonEnable = !(_quantity == 0 || _isAddToCart == true);
                return _isButtonEnable;
            }
        }

        public int GetLocalQuantity
        {
            get
            {
                return _localQuantity;
            }
            set
            {
                _localQuantity = value;
                if (_localQuantity > _quantity && _quantity != 0)
                {
                    _localQuantity = _quantity;
                    CartOverloadEvent(EventArgs.Empty);
                }
                Notify(Constant.GET_LOCAL_QUANTITY);
                Notify(Constant.GET_TOTAL_ROW_PRICE);
                Notify(Constant.GET_TOTAL_ROW_PRICE_NUMBER);
            }
        }

        public string GetTotalRowPrice
        {
            get
            {
                return String.Format(Constant.CHANGE_TO_CURRENCY, _localQuantity * _price);
            }
        }

        public int GetTotalRowPriceNumber
        {
            get
            {
                return _localQuantity * _price;
            }
        }

        /// Set product detail by string list
        public void SetProduct(List<string> data)
        {
            GetName = data[Constant.NUMBER_ZERO];
            GetPrice = int.Parse(data[Constant.NUMBER_ONE]);
            GetClassName = data[Constant.NUMBER_TWO];
            if (data[Constant.NUMBER_THREE] != string.Empty)
            {
                GetPicture = new Bitmap(Image.FromFile(data[Constant.NUMBER_THREE]));
                GetPicturePath = data[Constant.NUMBER_THREE];
            }
            GetDetail = data[Constant.NUMBER_FOUR];
        }

        /// Check the product is changes or not
        public bool IsModified(List<string> data)
        {
            return GetName == data[0] && GetPrice.ToString() == data[1] && GetClassName == data[Constant.NUMBER_TWO] && GetDetail == data[Constant.NUMBER_FOUR];
        }
    }
}

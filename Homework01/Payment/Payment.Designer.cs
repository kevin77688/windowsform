﻿namespace Homework01
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._paymentName = new System.Windows.Forms.Label();
            this._checkOut = new System.Windows.Forms.Button();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._creditCardNameLabel = new System.Windows.Forms.TableLayoutPanel();
            this._cardName = new System.Windows.Forms.Label();
            this._nameInsertPanel = new System.Windows.Forms.TableLayoutPanel();
            this._dashLine1 = new System.Windows.Forms.Label();
            this._firstName = new System.Windows.Forms.TextBox();
            this._lastName = new System.Windows.Forms.TextBox();
            this._creditCardNumberLabel = new System.Windows.Forms.TableLayoutPanel();
            this._creditCardNumber = new System.Windows.Forms.Label();
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._dashLine2 = new System.Windows.Forms.Label();
            this._dashLine3 = new System.Windows.Forms.Label();
            this._dashLine4 = new System.Windows.Forms.Label();
            this._firstCredit = new System.Windows.Forms.TextBox();
            this._secondCredit = new System.Windows.Forms.TextBox();
            this._thirdCredit = new System.Windows.Forms.TextBox();
            this._fourthCredit = new System.Windows.Forms.TextBox();
            this._creditCardDateLabel = new System.Windows.Forms.TableLayoutPanel();
            this._creditCardDate = new System.Windows.Forms.Label();
            this._tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this._slash = new System.Windows.Forms.Label();
            this._creditCardMonth = new System.Windows.Forms.ComboBox();
            this._creditCardYear = new System.Windows.Forms.ComboBox();
            this._cardCheckLayout = new System.Windows.Forms.TableLayoutPanel();
            this._cardCheckWord = new System.Windows.Forms.Label();
            this._cardCheckNumber = new System.Windows.Forms.TextBox();
            this._mailLayout = new System.Windows.Forms.TableLayoutPanel();
            this._mailWord = new System.Windows.Forms.Label();
            this._mailInsert = new System.Windows.Forms.TextBox();
            this._addressLayout = new System.Windows.Forms.TableLayoutPanel();
            this._addressWord = new System.Windows.Forms.Label();
            this._addressInsert = new System.Windows.Forms.TextBox();
            this._errorCode = new System.Windows.Forms.ErrorProvider(this.components);
            this._tableLayoutPanel1.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this._creditCardNameLabel.SuspendLayout();
            this._nameInsertPanel.SuspendLayout();
            this._creditCardNumberLabel.SuspendLayout();
            this._tableLayoutPanel3.SuspendLayout();
            this._creditCardDateLabel.SuspendLayout();
            this._tableLayoutPanel4.SuspendLayout();
            this._cardCheckLayout.SuspendLayout();
            this._mailLayout.SuspendLayout();
            this._addressLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._errorCode)).BeginInit();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 1;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.Controls.Add(this._paymentName, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._checkOut, 0, 2);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel2, 0, 1);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 3;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(379, 409);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _paymentName
            // 
            this._paymentName.AutoSize = true;
            this._paymentName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._paymentName.Font = new System.Drawing.Font("Microsoft YaHei", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._paymentName.Location = new System.Drawing.Point(2, 0);
            this._paymentName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._paymentName.Name = "_paymentName";
            this._paymentName.Size = new System.Drawing.Size(375, 40);
            this._paymentName.TabIndex = 0;
            this._paymentName.Text = "信用卡支付";
            this._paymentName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _checkOut
            // 
            this._checkOut.BackColor = System.Drawing.Color.Red;
            this._checkOut.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._checkOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this._checkOut.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._checkOut.ForeColor = System.Drawing.Color.White;
            this._checkOut.Location = new System.Drawing.Point(7, 369);
            this._checkOut.Margin = new System.Windows.Forms.Padding(7, 2, 7, 7);
            this._checkOut.Name = "_checkOut";
            this._checkOut.Size = new System.Drawing.Size(365, 33);
            this._checkOut.TabIndex = 1;
            this._checkOut.Text = "確定";
            this._checkOut.UseVisualStyleBackColor = false;
            this._checkOut.Click += new System.EventHandler(this.CheckConfirmButton);
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 1;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Controls.Add(this._creditCardNameLabel, 0, 0);
            this._tableLayoutPanel2.Controls.Add(this._creditCardNumberLabel, 0, 1);
            this._tableLayoutPanel2.Controls.Add(this._creditCardDateLabel, 0, 2);
            this._tableLayoutPanel2.Controls.Add(this._cardCheckLayout, 0, 3);
            this._tableLayoutPanel2.Controls.Add(this._mailLayout, 0, 4);
            this._tableLayoutPanel2.Controls.Add(this._addressLayout, 0, 5);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(2, 42);
            this._tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 6;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(375, 323);
            this._tableLayoutPanel2.TabIndex = 2;
            // 
            // _creditCardNameLabel
            // 
            this._creditCardNameLabel.ColumnCount = 3;
            this._creditCardNameLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardNameLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._creditCardNameLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardNameLabel.Controls.Add(this._cardName, 1, 0);
            this._creditCardNameLabel.Controls.Add(this._nameInsertPanel, 1, 1);
            this._creditCardNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardNameLabel.Location = new System.Drawing.Point(2, 2);
            this._creditCardNameLabel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._creditCardNameLabel.Name = "_creditCardNameLabel";
            this._creditCardNameLabel.RowCount = 2;
            this._creditCardNameLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._creditCardNameLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._creditCardNameLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardNameLabel.Size = new System.Drawing.Size(371, 49);
            this._creditCardNameLabel.TabIndex = 0;
            // 
            // _cardName
            // 
            this._cardName.AutoSize = true;
            this._cardName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cardName.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._cardName.Location = new System.Drawing.Point(15, 0);
            this._cardName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._cardName.Name = "_cardName";
            this._cardName.Size = new System.Drawing.Size(341, 24);
            this._cardName.TabIndex = 0;
            this._cardName.Text = "持卡人姓名*";
            this._cardName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _nameInsertPanel
            // 
            this._nameInsertPanel.ColumnCount = 3;
            this._nameInsertPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._nameInsertPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._nameInsertPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._nameInsertPanel.Controls.Add(this._dashLine1, 1, 0);
            this._nameInsertPanel.Controls.Add(this._firstName, 0, 0);
            this._nameInsertPanel.Controls.Add(this._lastName, 2, 0);
            this._nameInsertPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._nameInsertPanel.Location = new System.Drawing.Point(15, 26);
            this._nameInsertPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._nameInsertPanel.Name = "_nameInsertPanel";
            this._nameInsertPanel.RowCount = 1;
            this._nameInsertPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._nameInsertPanel.Size = new System.Drawing.Size(341, 21);
            this._nameInsertPanel.TabIndex = 1;
            // 
            // _dashLine1
            // 
            this._dashLine1.AutoSize = true;
            this._dashLine1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dashLine1.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this._dashLine1.Location = new System.Drawing.Point(156, 0);
            this._dashLine1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._dashLine1.Name = "_dashLine1";
            this._dashLine1.Size = new System.Drawing.Size(29, 21);
            this._dashLine1.TabIndex = 0;
            this._dashLine1.Text = "-";
            this._dashLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _firstName
            // 
            this._firstName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._firstName.Location = new System.Drawing.Point(2, 2);
            this._firstName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._firstName.Name = "_firstName";
            this._firstName.Size = new System.Drawing.Size(150, 22);
            this._firstName.TabIndex = 1;
            this._firstName.Tag = "name1";
            this._firstName.TextChanged += new System.EventHandler(this.CheckAllCorrect);
            this._firstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputNameAndAddress);
            this._firstName.Leave += new System.EventHandler(this.CheckNameWhenLeave);
            // 
            // _lastName
            // 
            this._lastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lastName.Location = new System.Drawing.Point(189, 2);
            this._lastName.Margin = new System.Windows.Forms.Padding(2, 2, 16, 2);
            this._lastName.Name = "_lastName";
            this._lastName.Size = new System.Drawing.Size(136, 22);
            this._lastName.TabIndex = 2;
            this._lastName.Tag = "name2";
            this._lastName.TextChanged += new System.EventHandler(this.CheckAllCorrect);
            this._lastName.Leave += new System.EventHandler(this.CheckNameWhenLeave);
            // 
            // _creditCardNumberLabel
            // 
            this._creditCardNumberLabel.ColumnCount = 3;
            this._creditCardNumberLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardNumberLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._creditCardNumberLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardNumberLabel.Controls.Add(this._creditCardNumber, 1, 0);
            this._creditCardNumberLabel.Controls.Add(this._tableLayoutPanel3, 1, 1);
            this._creditCardNumberLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardNumberLabel.Location = new System.Drawing.Point(2, 55);
            this._creditCardNumberLabel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._creditCardNumberLabel.Name = "_creditCardNumberLabel";
            this._creditCardNumberLabel.RowCount = 2;
            this._creditCardNumberLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._creditCardNumberLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._creditCardNumberLabel.Size = new System.Drawing.Size(371, 49);
            this._creditCardNumberLabel.TabIndex = 1;
            // 
            // _creditCardNumber
            // 
            this._creditCardNumber.AutoSize = true;
            this._creditCardNumber.BackColor = System.Drawing.SystemColors.Control;
            this._creditCardNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardNumber.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._creditCardNumber.Location = new System.Drawing.Point(15, 0);
            this._creditCardNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._creditCardNumber.Name = "_creditCardNumber";
            this._creditCardNumber.Size = new System.Drawing.Size(341, 24);
            this._creditCardNumber.TabIndex = 0;
            this._creditCardNumber.Text = "信用卡卡號*";
            this._creditCardNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 7;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel3.Controls.Add(this._dashLine2, 1, 0);
            this._tableLayoutPanel3.Controls.Add(this._dashLine3, 3, 0);
            this._tableLayoutPanel3.Controls.Add(this._dashLine4, 5, 0);
            this._tableLayoutPanel3.Controls.Add(this._firstCredit, 0, 0);
            this._tableLayoutPanel3.Controls.Add(this._secondCredit, 2, 0);
            this._tableLayoutPanel3.Controls.Add(this._thirdCredit, 4, 0);
            this._tableLayoutPanel3.Controls.Add(this._fourthCredit, 6, 0);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(15, 26);
            this._tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 1;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(341, 21);
            this._tableLayoutPanel3.TabIndex = 1;
            // 
            // _dashLine2
            // 
            this._dashLine2.AutoSize = true;
            this._dashLine2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dashLine2.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this._dashLine2.Location = new System.Drawing.Point(62, 0);
            this._dashLine2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._dashLine2.Name = "_dashLine2";
            this._dashLine2.Size = new System.Drawing.Size(29, 21);
            this._dashLine2.TabIndex = 0;
            this._dashLine2.Text = "-";
            this._dashLine2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _dashLine3
            // 
            this._dashLine3.AutoSize = true;
            this._dashLine3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dashLine3.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this._dashLine3.Location = new System.Drawing.Point(155, 0);
            this._dashLine3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._dashLine3.Name = "_dashLine3";
            this._dashLine3.Size = new System.Drawing.Size(29, 21);
            this._dashLine3.TabIndex = 1;
            this._dashLine3.Text = "-";
            this._dashLine3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _dashLine4
            // 
            this._dashLine4.AutoSize = true;
            this._dashLine4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dashLine4.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this._dashLine4.Location = new System.Drawing.Point(248, 0);
            this._dashLine4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._dashLine4.Name = "_dashLine4";
            this._dashLine4.Size = new System.Drawing.Size(29, 21);
            this._dashLine4.TabIndex = 2;
            this._dashLine4.Text = "-";
            this._dashLine4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _firstCredit
            // 
            this._firstCredit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._firstCredit.Location = new System.Drawing.Point(2, 2);
            this._firstCredit.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._firstCredit.Name = "_firstCredit";
            this._firstCredit.Size = new System.Drawing.Size(46, 22);
            this._firstCredit.TabIndex = 3;
            this._firstCredit.Tag = "4";
            this._firstCredit.TextChanged += new System.EventHandler(this.CheckInputOverflow);
            this._firstCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputOnlyNumber);
            this._firstCredit.Leave += new System.EventHandler(this.CheckNumberWhenLeave);
            // 
            // _secondCredit
            // 
            this._secondCredit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._secondCredit.Location = new System.Drawing.Point(95, 2);
            this._secondCredit.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._secondCredit.Name = "_secondCredit";
            this._secondCredit.Size = new System.Drawing.Size(46, 22);
            this._secondCredit.TabIndex = 4;
            this._secondCredit.Tag = "4";
            this._secondCredit.TextChanged += new System.EventHandler(this.CheckInputOverflow);
            this._secondCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputOnlyNumber);
            this._secondCredit.Leave += new System.EventHandler(this.CheckNumberWhenLeave);
            // 
            // _thirdCredit
            // 
            this._thirdCredit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._thirdCredit.Location = new System.Drawing.Point(188, 2);
            this._thirdCredit.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._thirdCredit.Name = "_thirdCredit";
            this._thirdCredit.Size = new System.Drawing.Size(46, 22);
            this._thirdCredit.TabIndex = 5;
            this._thirdCredit.Tag = "4";
            this._thirdCredit.TextChanged += new System.EventHandler(this.CheckInputOverflow);
            this._thirdCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputOnlyNumber);
            this._thirdCredit.Leave += new System.EventHandler(this.CheckNumberWhenLeave);
            // 
            // _fourthCredit
            // 
            this._fourthCredit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._fourthCredit.Location = new System.Drawing.Point(281, 2);
            this._fourthCredit.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._fourthCredit.Name = "_fourthCredit";
            this._fourthCredit.Size = new System.Drawing.Size(48, 22);
            this._fourthCredit.TabIndex = 6;
            this._fourthCredit.Tag = "4";
            this._fourthCredit.TextChanged += new System.EventHandler(this.CheckInputOverflow);
            this._fourthCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputOnlyNumber);
            this._fourthCredit.Leave += new System.EventHandler(this.CheckNumberWhenLeave);
            // 
            // _creditCardDateLabel
            // 
            this._creditCardDateLabel.ColumnCount = 3;
            this._creditCardDateLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardDateLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._creditCardDateLabel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._creditCardDateLabel.Controls.Add(this._creditCardDate, 1, 0);
            this._creditCardDateLabel.Controls.Add(this._tableLayoutPanel4, 1, 1);
            this._creditCardDateLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardDateLabel.Location = new System.Drawing.Point(2, 108);
            this._creditCardDateLabel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._creditCardDateLabel.Name = "_creditCardDateLabel";
            this._creditCardDateLabel.RowCount = 2;
            this._creditCardDateLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._creditCardDateLabel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._creditCardDateLabel.Size = new System.Drawing.Size(371, 49);
            this._creditCardDateLabel.TabIndex = 2;
            // 
            // _creditCardDate
            // 
            this._creditCardDate.AutoSize = true;
            this._creditCardDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardDate.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._creditCardDate.Location = new System.Drawing.Point(15, 0);
            this._creditCardDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._creditCardDate.Name = "_creditCardDate";
            this._creditCardDate.Size = new System.Drawing.Size(341, 24);
            this._creditCardDate.TabIndex = 0;
            this._creditCardDate.Text = "有效日期*(月/年)";
            this._creditCardDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _tableLayoutPanel4
            // 
            this._tableLayoutPanel4.ColumnCount = 3;
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel4.Controls.Add(this._slash, 1, 0);
            this._tableLayoutPanel4.Controls.Add(this._creditCardMonth, 0, 0);
            this._tableLayoutPanel4.Controls.Add(this._creditCardYear, 2, 0);
            this._tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel4.Location = new System.Drawing.Point(15, 26);
            this._tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel4.Name = "_tableLayoutPanel4";
            this._tableLayoutPanel4.RowCount = 1;
            this._tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel4.Size = new System.Drawing.Size(341, 21);
            this._tableLayoutPanel4.TabIndex = 1;
            // 
            // _slash
            // 
            this._slash.AutoSize = true;
            this._slash.Dock = System.Windows.Forms.DockStyle.Fill;
            this._slash.Location = new System.Drawing.Point(156, 0);
            this._slash.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._slash.Name = "_slash";
            this._slash.Size = new System.Drawing.Size(29, 21);
            this._slash.TabIndex = 0;
            this._slash.Text = "/";
            this._slash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _creditCardMonth
            // 
            this._creditCardMonth.BackColor = System.Drawing.SystemColors.Control;
            this._creditCardMonth.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._creditCardMonth.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._creditCardMonth.FormattingEnabled = true;
            this._creditCardMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this._creditCardMonth.Location = new System.Drawing.Point(2, 2);
            this._creditCardMonth.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._creditCardMonth.Name = "_creditCardMonth";
            this._creditCardMonth.Size = new System.Drawing.Size(150, 25);
            this._creditCardMonth.TabIndex = 1;
            this._creditCardMonth.TextChanged += new System.EventHandler(this.CheckAllCorrect);
            this._creditCardMonth.Leave += new System.EventHandler(this.CheckDateWhenLeave);
            // 
            // _creditCardYear
            // 
            this._creditCardYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this._creditCardYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._creditCardYear.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._creditCardYear.FormattingEnabled = true;
            this._creditCardYear.Items.AddRange(new object[] {
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028"});
            this._creditCardYear.Location = new System.Drawing.Point(189, 2);
            this._creditCardYear.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._creditCardYear.Name = "_creditCardYear";
            this._creditCardYear.Size = new System.Drawing.Size(140, 25);
            this._creditCardYear.TabIndex = 2;
            this._creditCardYear.TextChanged += new System.EventHandler(this.CheckAllCorrect);
            this._creditCardYear.Leave += new System.EventHandler(this.CheckDateWhenLeave);
            // 
            // _cardCheckLayout
            // 
            this._cardCheckLayout.ColumnCount = 3;
            this._cardCheckLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._cardCheckLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._cardCheckLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._cardCheckLayout.Controls.Add(this._cardCheckWord, 1, 0);
            this._cardCheckLayout.Controls.Add(this._cardCheckNumber, 1, 1);
            this._cardCheckLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cardCheckLayout.Location = new System.Drawing.Point(2, 161);
            this._cardCheckLayout.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._cardCheckLayout.Name = "_cardCheckLayout";
            this._cardCheckLayout.RowCount = 2;
            this._cardCheckLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._cardCheckLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._cardCheckLayout.Size = new System.Drawing.Size(371, 49);
            this._cardCheckLayout.TabIndex = 3;
            // 
            // _cardCheckWord
            // 
            this._cardCheckWord.AutoSize = true;
            this._cardCheckWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cardCheckWord.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._cardCheckWord.Location = new System.Drawing.Point(15, 0);
            this._cardCheckWord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._cardCheckWord.Name = "_cardCheckWord";
            this._cardCheckWord.Size = new System.Drawing.Size(341, 24);
            this._cardCheckWord.TabIndex = 0;
            this._cardCheckWord.Text = "背面末三碼*";
            this._cardCheckWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _cardCheckNumber
            // 
            this._cardCheckNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cardCheckNumber.Location = new System.Drawing.Point(15, 26);
            this._cardCheckNumber.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._cardCheckNumber.Name = "_cardCheckNumber";
            this._cardCheckNumber.Size = new System.Drawing.Size(331, 22);
            this._cardCheckNumber.TabIndex = 1;
            this._cardCheckNumber.Tag = "3";
            this._cardCheckNumber.TextChanged += new System.EventHandler(this.CheckInputOverflow);
            this._cardCheckNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputOnlyNumber);
            this._cardCheckNumber.Leave += new System.EventHandler(this.CheckSecurityCodeWhenLeave);
            // 
            // _mailLayout
            // 
            this._mailLayout.ColumnCount = 3;
            this._mailLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._mailLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mailLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._mailLayout.Controls.Add(this._mailWord, 1, 0);
            this._mailLayout.Controls.Add(this._mailInsert, 1, 1);
            this._mailLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mailLayout.Location = new System.Drawing.Point(2, 214);
            this._mailLayout.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._mailLayout.Name = "_mailLayout";
            this._mailLayout.RowCount = 2;
            this._mailLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._mailLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._mailLayout.Size = new System.Drawing.Size(371, 49);
            this._mailLayout.TabIndex = 4;
            // 
            // _mailWord
            // 
            this._mailWord.AutoSize = true;
            this._mailWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mailWord.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._mailWord.Location = new System.Drawing.Point(15, 0);
            this._mailWord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._mailWord.Name = "_mailWord";
            this._mailWord.Size = new System.Drawing.Size(341, 24);
            this._mailWord.TabIndex = 0;
            this._mailWord.Text = "Email*";
            this._mailWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _mailInsert
            // 
            this._mailInsert.BackColor = System.Drawing.Color.White;
            this._mailInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mailInsert.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._mailInsert.Location = new System.Drawing.Point(15, 26);
            this._mailInsert.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._mailInsert.Name = "_mailInsert";
            this._mailInsert.Size = new System.Drawing.Size(331, 25);
            this._mailInsert.TabIndex = 1;
            this._mailInsert.TextChanged += new System.EventHandler(this.CheckAllCorrect);
            this._mailInsert.Leave += new System.EventHandler(this.CheckMailWhenLeave);
            // 
            // _addressLayout
            // 
            this._addressLayout.ColumnCount = 3;
            this._addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._addressLayout.Controls.Add(this._addressWord, 1, 0);
            this._addressLayout.Controls.Add(this._addressInsert, 1, 1);
            this._addressLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addressLayout.Location = new System.Drawing.Point(2, 267);
            this._addressLayout.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._addressLayout.Name = "_addressLayout";
            this._addressLayout.RowCount = 2;
            this._addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._addressLayout.Size = new System.Drawing.Size(371, 54);
            this._addressLayout.TabIndex = 5;
            // 
            // _addressWord
            // 
            this._addressWord.AutoSize = true;
            this._addressWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addressWord.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._addressWord.Location = new System.Drawing.Point(15, 0);
            this._addressWord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._addressWord.Name = "_addressWord";
            this._addressWord.Size = new System.Drawing.Size(341, 27);
            this._addressWord.TabIndex = 0;
            this._addressWord.Text = "帳單地址*";
            this._addressWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _addressInsert
            // 
            this._addressInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addressInsert.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._addressInsert.Location = new System.Drawing.Point(15, 29);
            this._addressInsert.Margin = new System.Windows.Forms.Padding(2, 2, 12, 2);
            this._addressInsert.Name = "_addressInsert";
            this._addressInsert.Size = new System.Drawing.Size(331, 25);
            this._addressInsert.TabIndex = 1;
            this._addressInsert.Tag = "address";
            this._addressInsert.TextChanged += new System.EventHandler(this.CheckAllCorrect);
            this._addressInsert.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckInputNameAndAddress);
            this._addressInsert.Leave += new System.EventHandler(this.CheckAddressWhenLeave);
            // 
            // _errorCode
            // 
            this._errorCode.ContainerControl = this;
            // 
            // Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 409);
            this.Controls.Add(this._tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Payment";
            this.Text = "Payment";
            this._tableLayoutPanel1.ResumeLayout(false);
            this._tableLayoutPanel1.PerformLayout();
            this._tableLayoutPanel2.ResumeLayout(false);
            this._creditCardNameLabel.ResumeLayout(false);
            this._creditCardNameLabel.PerformLayout();
            this._nameInsertPanel.ResumeLayout(false);
            this._nameInsertPanel.PerformLayout();
            this._creditCardNumberLabel.ResumeLayout(false);
            this._creditCardNumberLabel.PerformLayout();
            this._tableLayoutPanel3.ResumeLayout(false);
            this._tableLayoutPanel3.PerformLayout();
            this._creditCardDateLabel.ResumeLayout(false);
            this._creditCardDateLabel.PerformLayout();
            this._tableLayoutPanel4.ResumeLayout(false);
            this._tableLayoutPanel4.PerformLayout();
            this._cardCheckLayout.ResumeLayout(false);
            this._cardCheckLayout.PerformLayout();
            this._mailLayout.ResumeLayout(false);
            this._mailLayout.PerformLayout();
            this._addressLayout.ResumeLayout(false);
            this._addressLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._errorCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.Label _paymentName;
        private System.Windows.Forms.Button _checkOut;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel _creditCardNameLabel;
        private System.Windows.Forms.Label _cardName;
        private System.Windows.Forms.TableLayoutPanel _nameInsertPanel;
        private System.Windows.Forms.Label _dashLine1;
        private System.Windows.Forms.TextBox _firstName;
        private System.Windows.Forms.TextBox _lastName;
        private System.Windows.Forms.TableLayoutPanel _creditCardNumberLabel;
        private System.Windows.Forms.Label _creditCardNumber;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.Label _dashLine2;
        private System.Windows.Forms.Label _dashLine3;
        private System.Windows.Forms.Label _dashLine4;
        private System.Windows.Forms.TextBox _firstCredit;
        private System.Windows.Forms.TextBox _secondCredit;
        private System.Windows.Forms.TextBox _thirdCredit;
        private System.Windows.Forms.TextBox _fourthCredit;
        private System.Windows.Forms.TableLayoutPanel _creditCardDateLabel;
        private System.Windows.Forms.Label _creditCardDate;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel4;
        private System.Windows.Forms.Label _slash;
        private System.Windows.Forms.TableLayoutPanel _cardCheckLayout;
        private System.Windows.Forms.Label _cardCheckWord;
        private System.Windows.Forms.TextBox _cardCheckNumber;
        private System.Windows.Forms.TableLayoutPanel _mailLayout;
        private System.Windows.Forms.Label _mailWord;
        private System.Windows.Forms.TextBox _mailInsert;
        private System.Windows.Forms.TableLayoutPanel _addressLayout;
        private System.Windows.Forms.Label _addressWord;
        private System.Windows.Forms.TextBox _addressInsert;
        private System.Windows.Forms.ComboBox _creditCardMonth;
        private System.Windows.Forms.ComboBox _creditCardYear;
        private System.Windows.Forms.ErrorProvider _errorCode;
    }
}
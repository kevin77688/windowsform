﻿using System.Collections.Generic;
using System.Xml;

namespace Homework01
{
    public class PaymentModel
    { 
        /// Remove the last character
        public string RemoveLastCharacter(string word)
        {
            return word.Remove(word.Length - 1);
        }

        /// Check Input restriction End
        /// 
        ///-------------------------------------------------
        ///
        ///Save file to xmlDocument
        public XmlDocument SaveFile(List<string> formData, XmlDocument lastData)
        {
            lastData.SelectSingleNode(Constant.FIRST_NAME_PATH).InnerText = formData[Constant.NUMBER_ZERO];
            lastData.SelectSingleNode(Constant.LAST_NAME_PATH).InnerText = formData[Constant.NUMBER_ONE];
            lastData.SelectSingleNode(Constant.CREDIT_CARD_FIRST_NUMBER_PATH).InnerText = formData[Constant.NUMBER_TWO];
            lastData.SelectSingleNode(Constant.CREDIT_CARD_SECOND_NUMBER_PATH).InnerText = formData[Constant.NUMBER_THREE];
            lastData.SelectSingleNode(Constant.CREDIT_CARD_THIRD_NUMBER_PATH).InnerText = formData[Constant.NUMBER_FOUR];
            lastData.SelectSingleNode(Constant.CREDIT_CARD_FOURTH_NUMBER_PATH).InnerText = formData[Constant.NUMBER_FIVE];
            lastData.SelectSingleNode(Constant.CREDIT_CARD_MONTH).InnerText = formData[Constant.NUMBER_SIX];
            lastData.SelectSingleNode(Constant.CREDIT_CARD_YEAR).InnerText = formData[Constant.NUMBER_SEVEN];
            lastData.SelectSingleNode(Constant.PAYMENT_MAIL_PATH).InnerText = formData[Constant.NUMBER_EIGHT];
            lastData.SelectSingleNode(Constant.PAYMENT_ADDRESS_PATH).InnerText = formData[Constant.NUMBER_NINE];
            return lastData;
        }
    }
}

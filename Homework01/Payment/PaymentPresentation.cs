﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml;

namespace Homework01
{
    public class PaymentPresentation
    {
        ///
        ///-------------------------------------------------
        /// Initial form Start
        private PaymentModel _paymentModel;
        readonly private bool _checkOutButton = false;

        public PaymentPresentation(PaymentModel paymentModel)
        {
            this._paymentModel = paymentModel;
        }

        /// Check if checkout button enable
        public bool IsCheckOutEnable()
        {
            return _checkOutButton;
        }
        /// Initial form End
        /// 
        ///-------------------------------------------------

        ///
        ///-------------------------------------------------
        /// Check Input restriction Start
        /// Check only input number
        public bool IsInputNumber(char key)
        {
            return !char.IsDigit(key) && !char.IsControl(key);
        }

        /// Check Input name and address
        public bool IsEnglishOrChinese(char character)
        {
            if (character == (char)Constant.NUMBER_EIGHT)
                return true;
            else
                return (character >= Constant.CHARACTER_LOWER_A_CODE && character <= Constant.CHARACTER_LAST_LETTER_LOWER_CODE || character >= Constant.CHARACTER_UPPER_A_CODE && character <= Constant.CHARACTER_LAST_LETTER_UPPER_CODE || character >= 0x4E00 && character <= 0x9FFF || character >= Constant.NUMBER_ONE_CODE && character <= Constant.NUMBER_ZERO_CODE || character == Constant.CHARACTER_DASH_CODE);
        }

        /// Check digit overflow or not
        public bool IsTextBoxLengthOverflow(int textLength, int maximum)
        {
            return textLength > maximum;
        }

        /// Remove the last character
        public string CheckRemoveLastCharacter(int textLength, int maximum, string word)
        {
            if (textLength > maximum)
                return _paymentModel.RemoveLastCharacter(word);
            else
                return word;
        }

        /// Check focus to next textbox or not
        public bool IsNextTextBoxFocus(bool checkTextBox, int stringLength)
        {
            return checkTextBox && (stringLength >= Constant.NUMBER_FOUR);
        }

        /// Check name textbox is null or not
        public bool IsNameTextBoxNull(int nameLength)
        {
            return nameLength == 0;
        }

        ///Check input number length
        public bool IsNumberTextBoxCorrect(string maximumLength, int textLength)
        {
            return textLength < Int32.Parse(maximumLength);
        }

        /// Check ComboBox selected is not null
        public bool IsComboBoxSelectNull(int selectRows)
        {
            return selectRows > -1;
        }

        /// Check Mail Valid
        public bool IsMailValid(string mail)
        {
            return (new EmailAddressAttribute().IsValid(mail) && mail != null);
        }
        /// Check Input restriction End
        /// 
        ///-------------------------------------------------

        ///
        ///-------------------------------------------------
        /// Validating Start
        /// Validating word is input or not
        public bool IsWordCorrect(int wordLength)
        {
            return wordLength == 0;
        }

        /// Validating number is input or not
        public bool IsNumberCorrect(int stringLength , string maximumLength)
        {
            return stringLength < Int32.Parse(maximumLength);
        }

        /// Validating End
        /// 
        ///-------------------------------------------------
        ///
        ///Save file to xmlDocument
        public XmlDocument SaveFile(List<string> formData, XmlDocument lastData)
        {
            return _paymentModel.SaveFile(formData, lastData);
        }
    }
}

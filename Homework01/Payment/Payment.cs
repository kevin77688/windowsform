﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Linq;

namespace Homework01
{
    public partial class Payment : Form
    {
        ///
        ///-------------------------------------------------
        /// Initial form Start
        public Payment(PaymentPresentation paymentPresentation)
        {
            InitializeComponent();
            this._paymentPresentation = paymentPresentation;
            InitialDictionary();
            _checkOut.Enabled = _paymentPresentation.IsCheckOutEnable();
            InitialData();
            SetDictionary();
        }
        private Dictionary<TextBox, TextBox> _swapToNext = new Dictionary<TextBox, TextBox>();
        private PaymentPresentation _paymentPresentation;
        private XmlDocument _lastData = new XmlDocument();
        private List<string> _formData = new List<string>();
        private List<Control> _allForm = new List<Control>();
        private Dictionary<string, bool> _getStatus = new Dictionary<string, bool>();

        /// Initial dictionary to find next textBox
        private void InitialDictionary()
        {
            List<TextBox> creditNumber = new List<TextBox>();
            TextBox[] textBoxes = { _firstCredit, _secondCredit, _thirdCredit, _fourthCredit };
            creditNumber.AddRange(textBoxes);
            for (int index = 0; index < Constant.NUMBER_THREE; index++)
                _swapToNext.Add(creditNumber[index], creditNumber[index + 1]);
            Control[] controls = { _firstName, _lastName, _firstCredit, _secondCredit, _thirdCredit, _fourthCredit, _creditCardMonth, _creditCardYear, _cardCheckNumber, _mailInsert, _addressInsert };
            _allForm.AddRange(controls);
            foreach (Control control in controls)
                _getStatus.Add(control.Name, false);
            _getStatus[_creditCardMonth.Name] = true;
            _getStatus[_creditCardYear.Name] = true;
        }

        /// Read last saved
        private void InitialData()
        {
            _lastData.Load(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + Constant.DEFAULT_TARGET_PATH);
            _firstName.Text = _lastData.SelectSingleNode(Constant.FIRST_NAME_PATH).InnerText;
            _lastName.Text = _lastData.SelectSingleNode(Constant.LAST_NAME_PATH).InnerText;
            _firstCredit.Text = _lastData.SelectSingleNode(Constant.CREDIT_CARD_FIRST_NUMBER_PATH).InnerText;
            _secondCredit.Text = _lastData.SelectSingleNode(Constant.CREDIT_CARD_SECOND_NUMBER_PATH).InnerText;
            _thirdCredit.Text = _lastData.SelectSingleNode(Constant.CREDIT_CARD_THIRD_NUMBER_PATH).InnerText;
            _fourthCredit.Text = _lastData.SelectSingleNode(Constant.CREDIT_CARD_FOURTH_NUMBER_PATH).InnerText;
            _creditCardMonth.SelectedIndex = Int32.Parse(_lastData.SelectSingleNode(Constant.CREDIT_CARD_MONTH).InnerText);
            _creditCardYear.SelectedIndex = Int32.Parse(_lastData.SelectSingleNode(Constant.CREDIT_CARD_YEAR).InnerText);
            _mailInsert.Text = _lastData.SelectSingleNode(Constant.PAYMENT_MAIL_PATH).InnerText;
            _addressInsert.Text = _lastData.SelectSingleNode(Constant.PAYMENT_ADDRESS_PATH).InnerText;
        }

        /// Set initial dictionary if purchase again
        private void SetDictionary()
        {
            if (_firstName.Text != Constant.NONE)
            {
                _getStatus[_firstName.Name] = true;
                _getStatus[_lastName.Name] = true;
                _getStatus[_firstCredit.Name] = true;
                _getStatus[_secondCredit.Name] = true;
                _getStatus[_thirdCredit.Name] = true;
                _getStatus[_fourthCredit.Name] = true;
                _getStatus[_creditCardMonth.Name] = true;
                _getStatus[_creditCardYear.Name] = true;
                _getStatus[_mailInsert.Name] = true;
                _getStatus[_addressInsert.Name] = true;
            }
        }
        /// Initial form End
        /// 
        ///-------------------------------------------------

        ///
        ///-------------------------------------------------
        /// Check Input restriction Start
        /// Set error provider
        private void SetError(Control control, string message = Constant.NONE)
        {
            if (message == Constant.NONE)
                _getStatus[control.Name] = true;
            else
                _getStatus[control.Name] = false;
            _errorCode.SetError(control, message);
        }

        /// Check only input number
        private void CheckInputOnlyNumber(object sender, KeyPressEventArgs e)
        {
            e.Handled = _paymentPresentation.IsInputNumber(e.KeyChar);
        }

        /// Check Input name and address
        private void CheckInputNameAndAddress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !_paymentPresentation.IsEnglishOrChinese(e.KeyChar);
        }

        /// Check digit overflow or not and check whether focus to next textbox
        private void CheckInputOverflow(object sender, EventArgs e)
        {
            TextBox currentTextBox = (TextBox)sender;
            CheckOutButton();
            int maximum = Int32.Parse((string)currentTextBox.Tag);
            currentTextBox.Text = _paymentPresentation.CheckRemoveLastCharacter(currentTextBox.Text.Length, maximum, currentTextBox.Text);
            if (_paymentPresentation.IsNextTextBoxFocus(_swapToNext.ContainsKey(currentTextBox), currentTextBox.Text.Length))
                _swapToNext[currentTextBox].Focus();
        }
        /// Check Input restriction End
        /// 
        ///-------------------------------------------------

        ///
        ///-------------------------------------------------
        /// Check when leave Start
        /// Check Name when leave form
        public void CheckNameWhenLeave(object sender, EventArgs e)
        {
            TextBox nameForm = (TextBox)sender;
            if (_paymentPresentation.IsNameTextBoxNull(nameForm.Text.Length))
                SetError(nameForm, Constant.NAME_INPUT_ERROR);
            else
                SetError(nameForm);
            CheckOutButton();
        }

        /// Check Card Number when leave form
        public void CheckNumberWhenLeave(object sender, EventArgs e)
        {
            TextBox number = (TextBox)sender;
            if (_paymentPresentation.IsNumberTextBoxCorrect((string)number.Tag, number.Text.Length))
                SetError(number, Constant.CREDIT_CARD_INPUT_ERROR);
            else
                SetError(number);
            CheckOutButton();
        }

        /// Check Month and year selected when leave form
        public void CheckDateWhenLeave(object sender, EventArgs e)
        {
            ComboBox currentSelected = (ComboBox)sender;
            if (_paymentPresentation.IsComboBoxSelectNull(currentSelected.SelectedIndex))
                SetError(currentSelected);
            else
                SetError(currentSelected, Constant.DATE_INPUT_ERROR);
            CheckOutButton();
        }

        /// Check CVV when leave form
        public void CheckSecurityCodeWhenLeave(object sender, EventArgs e)
        {
            TextBox number = (TextBox)sender;
            if (_paymentPresentation.IsNumberTextBoxCorrect((string)number.Tag, number.Text.Length))
                SetError(number, Constant.CREDIT_CARD_INPUT_ERROR);
            else
                SetError(number);
            CheckOutButton();
        }

        ///Check mail when leave
        private void CheckMailWhenLeave(object sender, EventArgs e)
        {
            TextBox mailInsert = (TextBox)sender;
            if (_paymentPresentation.IsMailValid(mailInsert.Text))
                SetError(mailInsert);
            else
                SetError(mailInsert, Constant.MAIL_INPUT_ERROR);
            CheckOutButton();
        }

        /// Check Address when leave form
        private void CheckAddressWhenLeave(object sender, EventArgs e)
        {
            TextBox nameForm = (TextBox)sender;
            if (_paymentPresentation.IsNameTextBoxNull(nameForm.Text.Length))
                SetError(nameForm, Constant.NAME_INPUT_ERROR);
            else
                SetError(nameForm);
            CheckOutButton();
        }
        /// Check when leave End
        /// 
        ///-------------------------------------------------

        ///
        ///-------------------------------------------------
        /// Validating Start
        /// Check All validated
        private void CheckAllCorrect(object sender, EventArgs e)
        {
            if (sender.Equals(_addressInsert))
                CheckAddressWhenLeave(sender, e);
            CheckOutButton();
        }

        /// Check enable checkout button
        private void CheckOutButton()
        {
            bool checkNoneError = true;
            foreach (Control control in _allForm)
            {
                if (!_getStatus[control.Name])
                    checkNoneError = false;
            }
            _checkOut.Enabled = checkNoneError;
        }
        /// Validating End
        /// 
        ///-------------------------------------------------

        ///
        ///-------------------------------------------------
        /// Save data to file start
        /// Save current to file
        private List<string> GetDataFromForm()
        {
            _formData.Clear();
            _formData.Add(_firstName.Text);
            _formData.Add(_lastName.Text);
            _formData.Add(_firstCredit.Text);
            _formData.Add(_secondCredit.Text);
            _formData.Add(_thirdCredit.Text);
            _formData.Add(_fourthCredit.Text);
            _formData.Add(_creditCardMonth.SelectedIndex.ToString());
            _formData.Add(_creditCardYear.SelectedIndex.ToString());
            _formData.Add(_mailInsert.Text);
            _formData.Add(_addressInsert.Text);
            return _formData;
        }

        /// Confirm button action
        private void CheckConfirmButton(object sender, EventArgs e)
        {
            _lastData = _paymentPresentation.SaveFile(GetDataFromForm(), _lastData);
            _lastData.Save(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + Constant.DEFAULT_TARGET_PATH);
            MessageBox.Show(Constant.PAYMENT_VALID);
            this.Close();
        }

        /// Save data End
        /// 
        ///-------------------------------------------------
    }
}

﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;

namespace Homework01
{
    public partial class InventorySystem : Form
    {
        private InventoryPresentation _inventoryPresentation;
        private BindingList<Goods> _productList;
        public InventorySystem(InventoryPresentation inventoryPresentation)
        {
            InitializeComponent();
            _inventoryPresentation = inventoryPresentation;
            _productList = _inventoryPresentation.GetAllProductList();
            InitialDataGrid();
        }

        /// Initial data grid 
        private void InitialDataGrid()
        {
            _inventoryGridView.AutoGenerateColumns = _inventoryPresentation.IsAutoGenerateColumns();
            _inventoryGridView.DataSource = _productList;
            _productDetailContent.DataBindings.Add(Constant.TEXT, _productList, Constant.GET_DETAIL);
            _itemPicture.DataBindings.Add(Constant.BACKGROUND, _productList, Constant.GET_PICTURE);
            _inventoryGridView.Columns[Constant.NUMBER_ZERO].DataPropertyName = Constant.GET_NAME;
            _inventoryGridView.Columns[Constant.NUMBER_ONE].DataPropertyName = Constant.GET_CLASS_NAME;
            _inventoryGridView.Columns[Constant.NUMBER_TWO].DataPropertyName = Constant.GET_PRICE;
            _inventoryGridView.Columns[Constant.NUMBER_THREE].DataPropertyName = Constant.GET_QUANTITY;
            _inventoryGridView.Columns[Constant.NUMBER_FOUR].DataPropertyName = Constant.GET_BUY_ICON;
        }

        /// Action while click replensihment button
        private void BuyButtonTrigger(object sender, DataGridViewCellEventArgs e)
        {
            if (_inventoryPresentation.IsSelectButtonValid(e.RowIndex, e.ColumnIndex))
            {
                List<string> _rowData = new List<string>();
                foreach (DataGridViewCell selectColumn in _inventoryGridView.Rows[e.RowIndex].Cells)
                    _rowData.Add((string)selectColumn.Value.ToString());
                ShopBuy _currentBuy = new ShopBuy(new ShopBuyPresentation(new ShopBuyModel(_rowData, _productList)));
                _currentBuy.ShowDialog();
            }
        }
    }
}

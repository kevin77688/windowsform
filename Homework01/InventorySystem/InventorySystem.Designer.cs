﻿namespace Homework01
{
    partial class InventorySystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._titleLabel = new System.Windows.Forms.Label();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._inventoryGridView = new System.Windows.Forms.DataGridView();
            this._productName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._productCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._gridPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._gridQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._shopBuyButton = new System.Windows.Forms.DataGridViewImageColumn();
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._itemPicture = new System.Windows.Forms.PictureBox();
            this._productNameTitle = new System.Windows.Forms.Label();
            this._productDetailTitle = new System.Windows.Forms.Label();
            this._productDetailContent = new System.Windows.Forms.Label();
            this._tableLayoutPanel1.SuspendLayout();
            this._mainLayoutPanel.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inventoryGridView)).BeginInit();
            this._tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._itemPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 1;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._tableLayoutPanel1.Controls.Add(this._mainLayoutPanel, 0, 0);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 1;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(533, 300);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _mainLayoutPanel
            // 
            this._mainLayoutPanel.ColumnCount = 1;
            this._mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainLayoutPanel.Controls.Add(this._titleLabel, 0, 0);
            this._mainLayoutPanel.Controls.Add(this._tableLayoutPanel2, 0, 1);
            this._mainLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this._mainLayoutPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._mainLayoutPanel.Name = "_mainLayoutPanel";
            this._mainLayoutPanel.RowCount = 2;
            this._mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._mainLayoutPanel.Size = new System.Drawing.Size(529, 296);
            this._mainLayoutPanel.TabIndex = 0;
            // 
            // _titleLabel
            // 
            this._titleLabel.AutoSize = true;
            this._titleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._titleLabel.Font = new System.Drawing.Font("Microsoft YaHei", 16F);
            this._titleLabel.Location = new System.Drawing.Point(2, 0);
            this._titleLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._titleLabel.Name = "_titleLabel";
            this._titleLabel.Size = new System.Drawing.Size(525, 44);
            this._titleLabel.TabIndex = 0;
            this._titleLabel.Text = "庫存管理系統";
            this._titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 2;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel2.Controls.Add(this._inventoryGridView, 0, 0);
            this._tableLayoutPanel2.Controls.Add(this._tableLayoutPanel3, 1, 0);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(2, 46);
            this._tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 1;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(525, 248);
            this._tableLayoutPanel2.TabIndex = 1;
            // 
            // _inventoryGridView
            // 
            this._inventoryGridView.AllowUserToAddRows = false;
            this._inventoryGridView.AllowUserToDeleteRows = false;
            this._inventoryGridView.AllowUserToResizeColumns = false;
            this._inventoryGridView.AllowUserToResizeRows = false;
            this._inventoryGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._inventoryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inventoryGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._productName,
            this._productCategory,
            this._gridPrice,
            this._gridQuantity,
            this._shopBuyButton});
            this._inventoryGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inventoryGridView.Location = new System.Drawing.Point(2, 2);
            this._inventoryGridView.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._inventoryGridView.Name = "_inventoryGridView";
            this._inventoryGridView.ReadOnly = true;
            this._inventoryGridView.RowHeadersVisible = false;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._inventoryGridView.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._inventoryGridView.RowTemplate.Height = 31;
            this._inventoryGridView.Size = new System.Drawing.Size(363, 244);
            this._inventoryGridView.TabIndex = 0;
            this._inventoryGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BuyButtonTrigger);
            // 
            // _productName
            // 
            this._productName.HeaderText = "商品名稱";
            this._productName.Name = "_productName";
            this._productName.ReadOnly = true;
            // 
            // _productCategory
            // 
            this._productCategory.HeaderText = "商品類別";
            this._productCategory.Name = "_productCategory";
            this._productCategory.ReadOnly = true;
            // 
            // _gridPrice
            // 
            this._gridPrice.HeaderText = "單價";
            this._gridPrice.Name = "_gridPrice";
            this._gridPrice.ReadOnly = true;
            // 
            // _gridQuantity
            // 
            this._gridQuantity.HeaderText = "數量";
            this._gridQuantity.Name = "_gridQuantity";
            this._gridQuantity.ReadOnly = true;
            this._gridQuantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _replenishmentButton
            // 
            this._shopBuyButton.HeaderText = "補貨";
            this._shopBuyButton.Name = "_replenishmentButton";
            this._shopBuyButton.ReadOnly = true;
            this._shopBuyButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 1;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel3.Controls.Add(this._itemPicture, 0, 1);
            this._tableLayoutPanel3.Controls.Add(this._productNameTitle, 0, 0);
            this._tableLayoutPanel3.Controls.Add(this._productDetailTitle, 0, 2);
            this._tableLayoutPanel3.Controls.Add(this._productDetailContent, 0, 3);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(369, 2);
            this._tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 4;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(154, 244);
            this._tableLayoutPanel3.TabIndex = 1;
            // 
            // _itemPicture
            // 
            this._itemPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._itemPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this._itemPicture.Location = new System.Drawing.Point(2, 25);
            this._itemPicture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._itemPicture.Name = "_itemPicture";
            this._itemPicture.Size = new System.Drawing.Size(150, 95);
            this._itemPicture.TabIndex = 0;
            this._itemPicture.TabStop = false;
            // 
            // _productNameTitle
            // 
            this._productNameTitle.AutoSize = true;
            this._productNameTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productNameTitle.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._productNameTitle.Location = new System.Drawing.Point(2, 0);
            this._productNameTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productNameTitle.Name = "_productNameTitle";
            this._productNameTitle.Size = new System.Drawing.Size(150, 23);
            this._productNameTitle.TabIndex = 1;
            this._productNameTitle.Text = "商品圖片";
            // 
            // _productDetailTitle
            // 
            this._productDetailTitle.AutoSize = true;
            this._productDetailTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productDetailTitle.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._productDetailTitle.Location = new System.Drawing.Point(2, 122);
            this._productDetailTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productDetailTitle.Name = "_productDetailTitle";
            this._productDetailTitle.Size = new System.Drawing.Size(150, 23);
            this._productDetailTitle.TabIndex = 2;
            this._productDetailTitle.Text = "商品介紹";
            // 
            // _productDetailContent
            // 
            this._productDetailContent.AutoSize = true;
            this._productDetailContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this._productDetailContent.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._productDetailContent.Location = new System.Drawing.Point(2, 145);
            this._productDetailContent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._productDetailContent.Name = "_productDetailContent";
            this._productDetailContent.Size = new System.Drawing.Size(150, 99);
            this._productDetailContent.TabIndex = 3;
            // 
            // InventorySystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 300);
            this.Controls.Add(this._tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "InventorySystem";
            this.Text = "CommingSoon";
            this._tableLayoutPanel1.ResumeLayout(false);
            this._mainLayoutPanel.ResumeLayout(false);
            this._mainLayoutPanel.PerformLayout();
            this._tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inventoryGridView)).EndInit();
            this._tableLayoutPanel3.ResumeLayout(false);
            this._tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._itemPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel _mainLayoutPanel;
        private System.Windows.Forms.Label _titleLabel;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.DataGridView _inventoryGridView;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.PictureBox _itemPicture;
        private System.Windows.Forms.Label _productNameTitle;
        private System.Windows.Forms.Label _productDetailTitle;
        private System.Windows.Forms.Label _productDetailContent;
        private System.Windows.Forms.DataGridViewTextBoxColumn _productName;
        private System.Windows.Forms.DataGridViewTextBoxColumn _productCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn _gridPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn _gridQuantity;
        private System.Windows.Forms.DataGridViewImageColumn _shopBuyButton;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Homework01
{
    public class InventoryPresentation
    {
        private InventoryModel _inventoryModel;
        private bool _autoGenerateColumn = false;

        public InventoryPresentation(InventoryModel inventoryModel)
        {
            _inventoryModel = inventoryModel;
        }

        /// Return all product list
        public BindingList<Goods> GetAllProductList()
        {
            return _inventoryModel.GetAllProductList();
        }

        /// Disable auto generate columns
        public bool IsAutoGenerateColumns()
        {
            return _autoGenerateColumn;
        }

        /// Check if the cursor click the right box
        public bool IsSelectButtonValid(int rows, int columns)
        {
            return rows >= 0 && columns == Constant.NUMBER_FOUR;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace Homework01
{
    public class InventoryModel
    {
        readonly private BindingList<Goods> _allProduct = new BindingList<Goods>();

        public InventoryModel(BindingList<Goods> getGoods)
        {
            _allProduct = getGoods;
        }

        /// Return all product list
        public BindingList<Goods> GetAllProductList()
        {
            return _allProduct;
        }
    }
}
